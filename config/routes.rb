Rails.application.routes.draw do

  mount RailsDb::Engine => '/rails/db', :as => 'rails_db' if Rails.env == "development"

  scope :api do
    devise_for :workers
  end

  namespace :api, defaults: {format: :json} do
    get 'heartbeat/alive' => 'heartbeat#alive'
    get 'heartbeat/signed_in' => 'heartbeat#signed_in'
    resources :attachments, only: [:create, :show]
    resources :appliances, except: [:new, :edit]
    resources :leaves, except: [:new, :edit]
    resources :perks, except: [:new, :edit]
    resources :recruitments, except: [:new, :edit]
    resources :working_times, except: [:new, :edit]
    resources :positions, except: [:new, :edit]
    resources :required_qualifications, except: [:new, :edit]
    resources :qualifications, except: [:new, :edit]
    resources :schedules, except: [:new, :edit]
    resources :workers, except: [:new, :edit] do
      collection do
        get 'get_working_times_for_user'
        post 'create_from_appliance'
        post 'reset_password'
      end
      member do
        get 'get_working_times_for_worker'
      end
    end
  end

  get '*path' => 'application#index'
  root 'application#index'
end
