require 'rails_helper'

RSpec.describe WorkingTime, type: :model do
  before { @wt = FactoryGirl.create(:working_time) }

  subject { @wt }

  it { should respond_to(:date) }
  it { should respond_to(:from) }
  it { should respond_to(:to) }

  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:from) }
  it { should validate_presence_of(:to) }

  it { should have_many(:positions) }
  it { should have_many(:schedules) }
  it { should have_many(:workers) }
end
