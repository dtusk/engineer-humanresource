require 'rails_helper'

RSpec.describe Appliance, type: :model do

  before { @appliance = FactoryGirl.create(:appliance) }

  subject { @appliance }

  it { should be_valid }
  it { should respond_to(:cv) }
  it { should respond_to(:approved) }
  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:pesel) }
  it { should respond_to(:street) }
  it { should respond_to(:house_nr) }
  it { should respond_to(:city) }
  it { should respond_to(:post_code) }
  it { should respond_to(:email) }

  
  it { should_not allow_value(nil).for(:approved)  }
  it { should validate_inclusion_of(:approved).in_array([true,false]) }

  it { should belong_to(:recruitment) }
  it { should have_and_belong_to_many(:qualifications) }

end
