require 'rails_helper'

RSpec.describe Attachment, type: :model do
  
  before { @attachment = FactoryGirl.create(:attachment) }

  subject { @attachment }

  it { should respond_to(:file) }

end
