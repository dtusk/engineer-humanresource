require 'rails_helper'

RSpec.describe Recruitment, type: :model do

  before { @recruitment = FactoryGirl.create(:recruitment) }

  subject { @recruitment }

  it { should be_valid }
  it { should respond_to(:status) }

  it { should_not allow_value(nil).for(:status)  }
  it { should validate_inclusion_of(:status).in_array([true, false]) }


  it { should have_many(:appliances) }
  it { should belong_to(:position) }

end
