require 'rails_helper'

RSpec.describe Leave, type: :model do
  before { @leave = FactoryGirl.create(:leave, worker: nil) }

  subject { @leave }

  it { should be_valid }
  it { should respond_to(:name) }
  it { should respond_to(:from) }
  it { should respond_to(:to) }
  it { should respond_to(:worker) }
  it { should belong_to(:worker) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:from) }
  it { should validate_presence_of(:to) }
end
