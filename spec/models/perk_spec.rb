require 'rails_helper'

RSpec.describe Perk, type: :model do

  before { @perk = FactoryGirl.create(:perk, worker: nil) }

  subject { @perk }

  it { should be_valid }
  it { should respond_to(:perks) }
  it { should respond_to(:date) }
  it { should respond_to(:reason) }

  it { should validate_presence_of(:perks) }
  it { should validate_numericality_of(:perks).is_greater_than_or_equal_to(0.0) }
  
  it { should validate_presence_of(:date) }

  it { should validate_presence_of(:reason) }

  it { should belong_to(:worker) }

end
