require 'rails_helper'

RSpec.describe Worker, type: :model do
  before { @worker = FactoryGirl.create(:worker) }

  subject { @worker }

  it { should be_valid }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:pesel) }
  it { should respond_to(:worker) }
  it { should respond_to(:hr) }
  it { should respond_to(:boss) }
  it { should respond_to(:admin) }

  it { should respond_to(:archive_worker) }
  it { should respond_to(:post_code) }
  it { should respond_to(:city) }
  it { should respond_to(:street) }
  it { should respond_to(:room_nr) }
  it { should respond_to(:house_nr) }


  it { should validate_presence_of(:first_name) }

  it { should validate_presence_of(:last_name) }

  it { should validate_presence_of(:pesel) }
  it { should validate_uniqueness_of(:pesel).case_insensitive }

  it { should validate_inclusion_of(:worker).in_array([true, false]) }
  it { should validate_inclusion_of(:hr).in_array([true, false]) }
  it { should validate_inclusion_of(:boss).in_array([true, false]) }
  it { should validate_inclusion_of(:admin).in_array([true, false]) }
  it { should validate_inclusion_of(:archive_worker).in_array([true, false]) }

  it { should validate_presence_of(:post_code) }
  it { should validate_presence_of(:city) }
  it { should validate_presence_of(:street) }
  it { should validate_presence_of(:house_nr) }

  it { should have_many(:leave) }
  it { should have_many(:perks) }
  it { should have_many(:schedules) }
  it { should have_and_belong_to_many(:positions) }
  it { should have_and_belong_to_many(:qualifications) }
end
