require 'rails_helper'

RSpec.describe Position, type: :model do

  before { @position = FactoryGirl.create(:position) }

  subject { @position }

  it { should be_valid }
  it { should respond_to(:name) }
  it { should respond_to(:salary) }
  it { should respond_to(:active) }

  it { should validate_presence_of(:name) }

  it { should validate_presence_of(:salary) }
  it { should validate_numericality_of(:salary).is_greater_than_or_equal_to(0.0) }

  it { should_not allow_value(nil).for(:active)  }
  it { should validate_inclusion_of(:active).in_array([true, false]) }

  it { should have_many(:recruitments) }

  it { should have_many(:required_qualifications) }
  it { should have_many(:working_times) }
  it { should have_and_belong_to_many(:workers) }

end
