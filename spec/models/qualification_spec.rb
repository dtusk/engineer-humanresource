require 'rails_helper'

RSpec.describe Qualification, type: :model do

  before { @qualification = FactoryGirl.create(:qualification) }

  subject { @qualification }

  it { should be_valid }
  it { should respond_to(:name) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }

  it { should have_many(:required_qualifications) }
  it { should have_many(:positions) }

  it { should have_and_belong_to_many(:workers) }
end
