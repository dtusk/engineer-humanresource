require 'rails_helper'

RSpec.describe Api::WorkingTimesController, type: :controller do
  let(:working_time) { FactoryGirl.create(:working_time) }
  let(:create_params) { { date: 1.day.ago, hours_of_work: 40} }

  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end

  describe "GET #show" do
    it "responds successfully with an HTTP 200 status code" do
      get :show, id: working_time.id

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end 
  end

  describe "POST #create" do
    it "responds successfully with an HTTP 201 status code" do
      request.accept = "application/json"
      post :create, working_time: create_params

      expect(response).to be_success
      expect(response).to have_http_status(201)
    end

    it "responds successfully with an HTTP 422 status code" do
      request.accept = "application/json"
      post :create, working_time: {hours_of_work: -1}

      expect(response).to have_http_status(422)
    end

    it "respond with created working time" do
      request.accept = "application/json"
      post :create, working_time: create_params

      expect(response.body).to include(create_params[:hours_of_work].to_s)
    end 
  end

  describe "PUT #update" do
    it "responds successfully with an HTTP 200 status code" do
      request.accept = "application/json"
      put :update, id: working_time.id, working_time: create_params


      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "responds successfully with an HTTP 422 status code" do
      request.accept = "application/json"
      put :update, id: working_time.id, working_time: {hours_of_work: -1}

      expect(response).to have_http_status(422)
    end

    it "respond with updated working time" do
      request.accept = "application/json"
      put :update, id: working_time.id, working_time: create_params

      expect(response.body).to include(create_params[:hours_of_work].to_s)
    end
  end

  describe "DELETE #destroy" do
    it "responds successfully with an HTTP 200 status code"
  end
end
