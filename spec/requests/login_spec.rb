require 'rails_helper'

RSpec.describe 'Login', type: :request do

  before { goto 'http://localhost:3000' }

  it "should be launched" do
    expect(a(href: '/')).to be_present
  end

  context "when I am trying log in" do
    it "should have a log in link" do
      expect(a(href: '/login')).to be_present
    end

    it "should log me in with valid data" do
      a(href: '/login').wait_until_present
      a(href: '/login').click
      text_field(name: 'username').set('00000000000')
      text_field(name: 'password').set('12345678')
      button(class: 'btn btn-default').click
      a(href: '/logout').wait_until_present
      expect(a(href: '/logout')).to be_present
    end

    it "should not log me in with invalid data" do
      a(href: '/logout').click
      a(href: '/login').wait_until_present
      a(href: '/login').click
      text_field(name: 'username').set('11876543210')
      text_field(name: 'password').set('teoiedenych')
      button(class: 'btn btn-default').click
      expect(text).to include("You've entered either wrong pesel or what is more likely to occur, password")
    end
  end
end
