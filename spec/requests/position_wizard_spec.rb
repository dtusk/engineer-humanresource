require 'rails_helper'

RSpec.describe "PositionWizard", type: :request do

  before { goto "http://localhost:3000" }

  it "should run website" do
    expect(a(href: '/')).to be_present
  end

  context "when browsing the website" do

    it "should be logged in" do
      a(href: '/login').wait_until_present
      a(href: '/login').click
      text_field(name: 'username').wait_until_present
      text_field(name: 'username').set('00000000000')
      text_field(name: 'password').set('12345678')
      button(class: 'btn btn-default').click
      a(href: '/logout').wait_until_present
      expect(a(href: '/logout')).to be_present
    end

    it "should have a wizard menu" do
      a(href: '#', text: 'Wizards').wait_until_present
      expect(a(href: '#', text: 'Wizards')).to be_present
    end

    it "should have a position wizard in wizard menu" do
      a(href: '#', text: 'Wizards').wait_until_present
      a(href: '#', text: 'Wizards').click
      expect(a(href: '/wizards/position-wizard/info')).to be_present
    end

    it "should go throught the position wizard" do
      a(href: '#', text: 'Wizards').wait_until_present
      a(href: '#', text: 'Wizards').click
      a(href: '/wizards/position-wizard/info').click
      button(text: 'Start wizard').click
      text_field(name: 'name').set('Nowa pozycja')
      text_field(name: 'salary').set('30000')
      text_field(class: 'ui-select-search').click
      button(text: 'Add next').click
    end
  end
end
