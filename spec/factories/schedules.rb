FactoryGirl.define do
  factory :schedule do
    position
    working_time
    worker
  end
end
