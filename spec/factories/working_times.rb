FactoryGirl.define do
  factory :working_time do
    date { Faker::Time.between(DateTime.now - 10, DateTime.now + 10) }
    from { Faker::Time.between(DateTime.now - 1, DateTime.now) }
    to { Faker::Time.between(DateTime.now - 1, DateTime.now) }
    positions { [FactoryGirl.create(:position)] }
  end

end
