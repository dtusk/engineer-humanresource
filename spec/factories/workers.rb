FactoryGirl.define do
  factory :worker do
    email { Faker::Internet.email }
    password { Faker::Internet.password(8) }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    pesel { Faker::Number.number(11) }
    worker [true, false].sample
    hr [true, false].sample
    boss false
    admin false
    archive_worker [true, false].sample
    post_code { Faker::Address.postcode }
    street { Faker::Address.street_name }
    house_nr { Faker::Address.building_number }
    room_nr { Faker::Address.building_number }
    city { Faker::Address.city }
    cv { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'cv.pdf'))  }
  end
end
