FactoryGirl.define do
  factory :perk do
    perks { Faker::Number.decimal(3) }
    date { Faker::Date.between(2.months.ago, Date.today) }
    reason { Faker::Lorem.paragraph }
    worker
  end

end
