FactoryGirl.define do
  factory :qualification do
    name { Faker::Company.buzzword }
  end

end
