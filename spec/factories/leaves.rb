FactoryGirl.define do
  factory :leave, :class => 'Leave' do
    name { Faker::Lorem.word}
    from { Faker::Date.between(2.months.ago, Date.today) }
    to { Faker::Date.between(2.months.ago, Date.today) }
    text { Faker::Lorem.sentences }
    approved [true, false, nil].sample
    worker
    file { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'cv.pdf'))  }
  end

end
