FactoryGirl.define do
  factory :position do
    name { Faker::Company.profession }
    salary { Faker::Number.decimal(4) }
    active [true,false].sample
    internship [true, false].sample
  end

end
