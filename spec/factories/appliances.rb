FactoryGirl.define do
  factory :appliance do
    cv { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'cv.pdf'))  }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    street { Faker::Address.street_name }
    pesel {Faker::Number.number(11)}
    house_nr { Faker::Address.building_number }
    city { Faker::Address.city }
    post_code { Faker::Address.zip_code }
    email { Faker::Internet.email }
    approved [true,false].sample
    recruitment
  end

end
