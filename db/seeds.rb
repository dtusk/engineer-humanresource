# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Worker.destroy_all(pesel: '00000000000')
Worker.create(first_name: 'Foo', last_name: 'Bar', pesel: '00000000000', worker: true,
              hr: true, boss: true, admin: true, password: '12345678', password_confirmation: '12345678', email: 'foo@bar.com',
              post_code: '00-000', street: 'foobarska', house_nr: '1', room_nr: '1', city: 'Foobar')

Worker.destroy_all(pesel: '10000000000')
Worker.create(first_name: 'Foo', last_name: 'Bar', pesel: '10000000000', worker: true,
              hr: false, boss: false, admin: false, password: '12345678', password_confirmation: '12345678', email: 'foo1@bar.com',
              post_code: '00-000', street: 'foobarska', house_nr: '1', room_nr: '1', city: 'Foobar')

Worker.destroy_all(pesel: '20000000000')
Worker.create(first_name: 'Foo', last_name: 'Bar', pesel: '20000000000', worker: false,
              hr: false, boss: false, admin: true, password: '12345678', password_confirmation: '12345678', email: 'foo2@bar.com',
              post_code: '00-000', street: 'foobarska', house_nr: '1', room_nr: '1', city: 'Foobar')

Worker.destroy_all(pesel: '30000000000')
Worker.create(first_name: 'Foo', last_name: 'Bar', pesel: '30000000000', worker: true,
              hr: true, boss: false, admin: false, password: '12345678', password_confirmation: '12345678', email: 'foo3@bar.com',
              post_code: '00-000', street: 'foobarska', house_nr: '1', room_nr: '1', city: 'Foobar')