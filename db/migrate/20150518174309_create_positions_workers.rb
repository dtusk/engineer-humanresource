class CreatePositionsWorkers < ActiveRecord::Migration
  def change
    create_table :positions_workers do |t|
      t.references :position, index: true, foreign_key: true
      t.references :worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
