class CreateAppliances < ActiveRecord::Migration
  def change
    create_table :appliances do |t|
      t.string :cv
      t.references :position, index: true, foreign_key: true
      t.references :recruitment, index: true, foreign_key: true
      t.boolean :approved

      t.timestamps null: false
    end
  end
end
