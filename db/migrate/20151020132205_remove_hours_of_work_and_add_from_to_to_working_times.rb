class RemoveHoursOfWorkAndAddFromToToWorkingTimes < ActiveRecord::Migration
  def up
    remove_column :working_times, :hours_of_work
    add_column :working_times, :from, :time
    add_column :working_times, :to, :time
  end

  def down
    add_column :working_times, :hours_of_work, :integer
    remove_column :working_times, :from
    remove_column :working_times, :to
  end
end
