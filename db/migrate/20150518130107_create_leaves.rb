class CreateLeaves < ActiveRecord::Migration
  def change
    create_table :leaves do |t|
      t.string :name
      t.date :from
      t.date :to
      t.boolean :approved
      t.references :worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
