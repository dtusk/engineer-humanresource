class CreateWorkingTimes < ActiveRecord::Migration
  def change
    create_table :working_times do |t|
      t.date :date
      t.integer :hours_of_work

      t.timestamps null: false
    end
  end
end
