class AddWorkerToWorkers < ActiveRecord::Migration
  def change
    add_column :workers, :worker, :boolean, null: false, default: true
  end
end
