class AddPropositionsOfQualificationsToAppliance < ActiveRecord::Migration
  def change
    add_column :appliances, :propositions_of_qualifications, :text
  end
end
