class MergeAddressIntoWorker < ActiveRecord::Migration
  def change
    add_column :workers, :post_code, :string
    add_column :workers, :street, :string
    add_column :workers, :house_nr, :string
    add_column :workers, :room_nr, :string
    add_column :workers, :city, :string
    drop_table :addresses_workers, force: :cascade
    drop_table :addresses, force: :cascade
  end
end
