class CreateQualificationsWorkers < ActiveRecord::Migration
  def change
    create_table :qualifications_workers do |t|
      t.references :qualification, index: true, foreign_key: true
      t.references :worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
