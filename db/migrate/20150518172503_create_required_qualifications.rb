class CreateRequiredQualifications < ActiveRecord::Migration
  def change
    create_table :required_qualifications do |t|
      t.references :position, index: true, foreign_key: true
      t.references :qualification, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
