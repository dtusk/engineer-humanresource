class CreateAddressesWorkers < ActiveRecord::Migration
  def change
    create_table :addresses_workers do |t|
      t.references :address, index: true, foreign_key: true
      t.references :worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
