class AddAddressFieldsToAppliances < ActiveRecord::Migration
  def change
    add_column :appliances, :first_name, :string
    add_column :appliances, :last_name, :string
    add_column :appliances, :street, :string
    add_column :appliances, :pesel, :string
    add_column :appliances, :house_nr, :string
    add_column :appliances, :city, :string
    add_column :appliances, :post_code, :string
  end
end
