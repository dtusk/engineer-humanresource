class RemovePositionIdFromAppliance < ActiveRecord::Migration
  def change
    remove_column :appliances, :position_id, :integer
  end
end
