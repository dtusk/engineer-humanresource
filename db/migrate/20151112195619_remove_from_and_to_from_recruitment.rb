class RemoveFromAndToFromRecruitment < ActiveRecord::Migration
  def change
    remove_column :recruitments, :from, :string
    remove_column :recruitments, :to, :string
  end
end
