class CreateAppliancesQualifications < ActiveRecord::Migration
  def change
    create_table :appliances_qualifications do |t|
      t.references :appliance
      t.references :qualification
    end
  end
end
