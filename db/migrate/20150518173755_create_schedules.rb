class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.references :position, index: true, foreign_key: true
      t.references :worker, index: true, foreign_key: true
      t.references :working_time, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
