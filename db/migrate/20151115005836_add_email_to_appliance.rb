class AddEmailToAppliance < ActiveRecord::Migration
  def change
    add_column :appliances, :email, :string
  end
end
