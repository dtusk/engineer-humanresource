class AddInternshipToPositions < ActiveRecord::Migration
  def change
    add_column :positions, :internship, :boolean, null: false, default: false
  end
end
