class RemoveNipFromWorker < ActiveRecord::Migration
  def up
    remove_column :workers, :nip
  end

  def down
    add_column :workers, :nip, :string
  end
end
