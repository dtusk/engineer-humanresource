class CreatePerks < ActiveRecord::Migration
  def change
    create_table :perks do |t|
      t.decimal :perks, precision: 8, scale: 2
      t.date :date
      t.text :reason
      t.references :worker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
