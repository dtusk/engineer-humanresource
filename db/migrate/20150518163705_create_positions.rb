class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :name
      t.decimal :salary, precision: 8, scale: 2
      t.boolean :active, null: false, default: false

      t.timestamps null: false
    end
  end
end
