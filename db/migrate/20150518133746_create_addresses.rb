class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :post_code
      t.string :street
      t.string :house_nr
      t.string :room_nr
      t.string :city

      t.timestamps null: false
    end
  end
end
