class AddFileToLeaves < ActiveRecord::Migration
  def change
    add_column :leaves, :file, :string
  end
end
