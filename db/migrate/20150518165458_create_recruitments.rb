class CreateRecruitments < ActiveRecord::Migration
  def change
    create_table :recruitments do |t|
      t.references :position, index: true, foreign_key: true
      t.date :from
      t.date :to
      t.boolean :status

      t.timestamps null: false
    end
  end
end
