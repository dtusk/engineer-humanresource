# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151206095956) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appliances", force: :cascade do |t|
    t.string   "cv"
    t.integer  "recruitment_id"
    t.boolean  "approved"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "street"
    t.string   "pesel"
    t.string   "house_nr"
    t.string   "city"
    t.string   "post_code"
    t.string   "email"
    t.text     "propositions_of_qualifications"
  end

  add_index "appliances", ["recruitment_id"], name: "index_appliances_on_recruitment_id", using: :btree

  create_table "appliances_qualifications", force: :cascade do |t|
    t.integer "appliance_id"
    t.integer "qualification_id"
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leaves", force: :cascade do |t|
    t.string   "name"
    t.date     "from"
    t.date     "to"
    t.boolean  "approved"
    t.integer  "worker_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "text"
    t.string   "file"
  end

  add_index "leaves", ["worker_id"], name: "index_leaves_on_worker_id", using: :btree

  create_table "perks", force: :cascade do |t|
    t.decimal  "perks",      precision: 8, scale: 2
    t.date     "date"
    t.text     "reason"
    t.integer  "worker_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "perks", ["worker_id"], name: "index_perks_on_worker_id", using: :btree

  create_table "positions", force: :cascade do |t|
    t.string   "name"
    t.decimal  "salary",     precision: 8, scale: 2
    t.boolean  "active",                             default: false, null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.boolean  "internship",                         default: false, null: false
  end

  create_table "positions_workers", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "worker_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "positions_workers", ["position_id"], name: "index_positions_workers_on_position_id", using: :btree
  add_index "positions_workers", ["worker_id"], name: "index_positions_workers_on_worker_id", using: :btree

  create_table "qualifications", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "qualifications_workers", force: :cascade do |t|
    t.integer  "qualification_id"
    t.integer  "worker_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "qualifications_workers", ["qualification_id"], name: "index_qualifications_workers_on_qualification_id", using: :btree
  add_index "qualifications_workers", ["worker_id"], name: "index_qualifications_workers_on_worker_id", using: :btree

  create_table "recruitments", force: :cascade do |t|
    t.integer  "position_id"
    t.boolean  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "recruitments", ["position_id"], name: "index_recruitments_on_position_id", using: :btree

  create_table "required_qualifications", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "qualification_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "required_qualifications", ["position_id"], name: "index_required_qualifications_on_position_id", using: :btree
  add_index "required_qualifications", ["qualification_id"], name: "index_required_qualifications_on_qualification_id", using: :btree

  create_table "schedules", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "worker_id"
    t.integer  "working_time_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "schedules", ["position_id"], name: "index_schedules_on_position_id", using: :btree
  add_index "schedules", ["worker_id"], name: "index_schedules_on_worker_id", using: :btree
  add_index "schedules", ["working_time_id"], name: "index_schedules_on_working_time_id", using: :btree

  create_table "workers", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "pesel"
    t.boolean  "hr",                     default: false, null: false
    t.boolean  "boss",                   default: false, null: false
    t.boolean  "admin",                  default: false, null: false
    t.boolean  "archive_worker",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "worker",                 default: true,  null: false
    t.string   "post_code"
    t.string   "street"
    t.string   "house_nr"
    t.string   "room_nr"
    t.string   "city"
    t.string   "cv"
  end

  add_index "workers", ["email"], name: "index_workers_on_email", unique: true, using: :btree
  add_index "workers", ["pesel"], name: "index_workers_on_pesel", unique: true, using: :btree
  add_index "workers", ["reset_password_token"], name: "index_workers_on_reset_password_token", unique: true, using: :btree

  create_table "working_times", force: :cascade do |t|
    t.date     "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.time     "from"
    t.time     "to"
  end

  add_foreign_key "appliances", "recruitments"
  add_foreign_key "leaves", "workers"
  add_foreign_key "perks", "workers"
  add_foreign_key "positions_workers", "positions"
  add_foreign_key "positions_workers", "workers"
  add_foreign_key "qualifications_workers", "qualifications"
  add_foreign_key "qualifications_workers", "workers"
  add_foreign_key "recruitments", "positions"
  add_foreign_key "required_qualifications", "positions"
  add_foreign_key "required_qualifications", "qualifications"
  add_foreign_key "schedules", "positions"
  add_foreign_key "schedules", "workers"
  add_foreign_key "schedules", "working_times"
end
