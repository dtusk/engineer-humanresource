// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require turbolinks
//= require moment
//= require jquery
//= require jquery_ujs
//= require pdfjs-dist/build/pdf.combined
//= require angular/angular
//= require angular-animate/angular-animate
//= require Chart.js/Chart
//= require angular-chart.js/dist/angular-chart
//= require angular-pdf-viewer/dist/angular-pdf-viewer.min
//= require fullcalendar/dist/fullcalendar
//= require angular-bootstrap-checkbox/angular-bootstrap-checkbox
//= require angular-advanced-searchbox.js.erb
//= require angular-advanced-searchbox-tpls.js.erb
//= require angular-ui-select/dist/select
//= require angular-ui-router/release/angular-ui-router
//= require angular-ui-calendar/src/calendar
//= require angular-resource/angular-resource
//= require angular-devise/lib/devise
//= require select2/dist/js/select2.full
//= require angular-file-upload/dist/angular-file-upload
//= require ng-notify/dist/ng-notify.min
//= require app/app
//= require angular-utils-pagination/dirPagination
//= require bootstrap/dist/js/bootstrap
//= require angular-bootstrap/ui-bootstrap
//= require angular-bootstrap/ui-bootstrap-tpls
//= require_tree ./app/directives
//= require_tree ./app/models
//= require_tree ./app/services
//= require_tree ./app/controllers

