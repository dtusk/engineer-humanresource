this.appServices.service('SessionService', ['$http', function($http) {
    this.isServerAlive = function(cb) {
        $http.get('/api/heartbeat/alive').then(function(success) {
            cb(true);
        }, function(error) {
            cb(false);
        });
    }


    this.isLoggedIn = function(cb) {
        $http.get('/api/heartbeat/signed_in').then(function(success) {
            cb(true);
        }, function(error) {
            cb(false);
        });
    }
}]);
