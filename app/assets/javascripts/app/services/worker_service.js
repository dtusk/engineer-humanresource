this.appServices.service('WorkerService', ['Worker', '$http', function(Worker, $http) {


    this.getWorkingTimesForWorker = function(cb) {
        $http.get('/api/workers/get_working_times_for_user').then(function(res) { cb(res['data']) });
    };
    this.getWorkingTimesForUser = function(workerId, cb) {
        $http.get('/api/workers/' + workerId + '/get_working_times_for_worker').then(function(res) { cb(res['data']) });
    };
    this.createFromApliance = function(worker_params, success, failure) {
        $http.post('/api/workers/create_from_appliance', { worker: worker_params }).then(function(res) { success(res['data']) }, function(res) { failure(res['data'])});
    };
    this.remindPassword = function(pesel, cb){
        $http.post('/api/workers/reset_password', { worker: {pesel: pesel} }).then(function(res) { cb(res['data']) });
    };
}]);
