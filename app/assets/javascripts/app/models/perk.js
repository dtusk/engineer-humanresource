'use strict';

this.appModels.factory('Perk', [
    '$resource', function ($resource) {
        return $resource('/api/perks/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);