'use strict';

this.appModels.factory('RequiredQualification', [
    '$resource', function ($resource) {
        return $resource('/api/required_qualifications/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);