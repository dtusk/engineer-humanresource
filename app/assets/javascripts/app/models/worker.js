'use strict';

this.appModels.factory('Worker', [
    '$resource', function ($resource) {
        return $resource('/api/workers/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
