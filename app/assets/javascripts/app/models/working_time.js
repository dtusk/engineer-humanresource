'use strict';

this.appModels.factory('WorkingTime', [
    '$resource', function ($resource) {
        return $resource('/api/working_times/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);