'use strict';

this.appModels.factory('Leave', [
    '$resource', function ($resource) {
        return $resource('/api/leaves/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);