'use strict';

this.appModels.factory('Position', ['$resource', function ($resource) {
        return $resource('/api/positions/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);