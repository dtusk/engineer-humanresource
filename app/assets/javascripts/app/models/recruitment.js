'use strict';

this.appModels.factory('Recruitment', [
    '$resource', function ($resource) {
        return $resource('/api/recruitments/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);