'use strict';

this.appModels.factory('Qualification', ['$resource', function ($resource) {
        return $resource('/api/qualifications/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);