'use strict';

this.appModels.factory('Appliance', [
    '$resource', function ($resource) {
        return $resource('/api/appliances/:id', {
            id: "@id"
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);