'use strict';

this.appControllers.controller('SessionsLogoutController', ['Auth', '$state', '$scope', '$rootScope', 'ngNotify', '$interval',
    function (Auth, $state, $scope, $rootScope, ngNotify, $interval) {
        Auth.logout().then(function (old) {
            $rootScope.sessionRoot = null;
            $rootScope.isAuthenticated = Auth.isAuthenticated();
            delete $rootScope.ifHR;
            delete $rootScope.ifBoss;
            delete $rootScope.ifAdmin;
            delete $rootScope.ifWorker;
            delete $rootScope.currentUser;
            $state.go('main');
            ngNotify.set("You've been loged out", 'error');
            clearInterval(window.intervalIsLoggedIn);
        });
    }]);