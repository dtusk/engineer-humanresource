'use strict';

this.appControllers.controller('StatisticController', [
    'Position', 'Qualification', 'Perk', 'Worker', 'WorkingTime', 'Leave', 'Recruitment', 'Appliance', '$scope', '$rootScope', '$stateParams', '$state', 'ngNotify', function (Position, Qualification, Perk, Worker, WorkingTime, Leave, Recruitment, Appliance, $scope, $rootScope, $stateParams, $state, ngNotify) {
        this.init = function () {
            $scope.todayMoment = moment().format('YYYY-MM-DD');
            $scope.today = new Date();
            var month = $scope.today.getMonth();
            var year = $scope.today.getFullYear();
            $scope.newDate = new Date(year, month, 1);
            $scope.monthBeforeDate = new Date(year, month - 1, $scope.newDate.getDate());
            var getDaysOfMonth = function (date) {
                return new Date(date.getFullYear(), date.getMonth(), 0).getDate();
            }
            $scope.salary = function () {
                var result = 0;
                if (new Date($rootScope.currentUser.created_at) > new Date(year, month + 1, 0)) {
                    result = parseFloat($rootScope.currentUser.positions[0].salary);
                } else result = 0;
                return result;
            }
            $scope.getSum = function () {
                var sum = 0;
                for (var i = 0; i < $rootScope.currentUser.perks.length; i++) {
                    var perk = parseFloat($rootScope.currentUser.perks[i].perks);
                    sum = perk + sum;
                }
                ;
                return sum;
            };
            $scope.getSumFromLastMonth = function () {
                var sum = 0;
                for (var i = 0; i < $rootScope.currentUser.perks.length; i++) {
                    var tmp = new Date($rootScope.currentUser.perks[i].date);
                    if (tmp < $scope.newDate && tmp > $scope.monthBeforeDate) {
                        var perk = parseFloat($rootScope.currentUser.perks[i].perks);
                        sum = perk + sum;
                    }
                }
                ;
                $scope.sum = $scope.salary() + sum;
                return sum;
            };
            $scope.days = function (from, to) {
                var fromDate = new Date(from);
                var toDate = new Date(to);
                var fromDateDay = fromDate.getDate();
                var toDateDay = toDate.getDate();
                if (toDateDay < fromDateDay) {
                    return toDateDay - fromDateDay + getDaysOfMonth(toDate);
                } else
                    return toDateDay - fromDateDay;
            }
            $scope.filterDates = function (leave) {
                return leave.date < $scope.newDate && leave.date > $scope.monthBeforeDate;
            }
            $scope.filterApproved = function (leave) {
                return leave.approved == true;
            }

        };
        this.initAll = function () {
            $scope.filterNotArchivedWorker = function (leave) {
                return !leave.worker.archive_worker;
            }
            $scope.filterNotArchived = function (worker) {
                return !worker.archive_worker;
            }
            $scope.today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), new Date().getHours() + 1);
            var month = $scope.today.getMonth();
            var year = $scope.today.getFullYear();
            $scope.newDate = new Date(year, month, 1);
            $scope.monthBeforeDate = new Date(year, month - 1, $scope.newDate.getDate());
            $scope.currentLeavesLabels = [];
            $scope.currentLeavesSum = 0;
            $scope.currentLeavesData = [];
            $scope.leavesFromPastMonthLabels = [];
            $scope.leavesFromPastMonthSum = 0;
            $scope.leavesFromPastMonthData = [];
            $scope.perksLabels = [];
            $scope.perksSeries = ['Ammount'];
            $scope.perksTmpData = [];
            $scope.perksData = [$scope.perksTmpData];
            $scope.positionLabels = [];
            $scope.positionData = [];
            $scope.positionSalarySeries = ['per worker', 'per position'];
            $scope.positionSalaryLabels = [];
            $scope.positionSalaryTmpData = [];
            $scope.positionSalaryTmpAllData = [];
            $scope.positionSalaryData = [$scope.positionSalaryTmpData, $scope.positionSalaryTmpAllData];
            $scope.perks = true;
            $scope.salaries = true;
            $scope.leaves = true;
            $scope.recruitment = true;
            var found = false;
            $scope.allPerks = Perk.query();
            $scope.allWorkers = Worker.query(function () {
                for (var i = 0; i < $scope.allWorkers.length; i++) {
                    var perksSum = 0;
                    if ($scope.allWorkers[i].perks.length > 0 && $scope.allWorkers[i].archive_worker == false) {
                        for (var k = 0; k < $scope.allWorkers[i].perks.length; k++) {
                            if (new Date($scope.allWorkers[i].perks[k].date) < $scope.newDate && new Date($scope.allWorkers[i].perks[k].date) > $scope.monthBeforeDate) {
                                perksSum = perksSum + parseFloat($scope.allWorkers[i].perks[k].perks);
                                found = true;
                            }else found = false;
                        }
                        if(found == true){
                            $scope.perksLabels.push($scope.allWorkers[i].first_name + " " + $scope.allWorkers[i].last_name);
                            $scope.perksTmpData.push(perksSum)
                        }
                    }
                    if ($scope.allWorkers[i].archive_worker == false) {
                        for (var j = 0; j < $scope.allWorkers[i].leave.length; j++) {
                            if ((new Date($scope.allWorkers[i].leave[j].from) <= $scope.today && $scope.today <= new Date($scope.allWorkers[i].leave[j].to)) && $scope.allWorkers[i].leave[j].approved == true) {
                                $scope.currentLeavesSum = $scope.currentLeavesSum + 1;
                            }

                            if ((((new Date($scope.allWorkers[i].leave[j].from) || new Date($scope.allWorkers[i].leave[j].to)) >= $scope.monthBeforeDate) && ((new Date($scope.allWorkers[i].leave[j].from) || new Date($scope.allWorkers[i].leave[j].to)) <= $scope.newDate)) && $scope.allWorkers[i].leave[j].approved == true) {
                                $scope.leavesFromPastMonthSum = $scope.leavesFromPastMonthSum + $scope.days($scope.allWorkers[i].leave[j].from, $scope.allWorkers[i].leave[j].to)
                            }
                        }
                        $scope.leavesFromPastMonthLabels.push($scope.allWorkers[i].first_name + " " + $scope.allWorkers[i].last_name);
                        $scope.leavesFromPastMonthData.push($scope.leavesFromPastMonthSum);
                        $scope.leavesFromPastMonthSum = 0;
                    }
                }
                $scope.currentLeavesLabels.push("Currently on leave");
                $scope.currentLeavesData.push($scope.currentLeavesSum);
                $scope.currentLeavesLabels.push("Not on leave");
                $scope.currentLeavesData.push($scope.allWorkers.length - $scope.currentLeavesSum);
                $scope.currentLeavesSum = 0;
            });
            $scope.allPositions = Position.query(function () {
                for (var i = 0; i < $scope.allPositions.length; i++) {
                    if ($scope.allPositions[i].workers.length > 0) {
                        $scope.positionLabels.push($scope.allPositions[i].name);
                        $scope.positionData.push($scope.allPositions[i].workers.length);
                        $scope.positionSalaryLabels.push($scope.allPositions[i].name);
                        $scope.positionSalaryTmpData.push($scope.allPositions[i].salary);
                        $scope.positionSalaryTmpAllData.push($scope.salarySummedForPosition($scope.allPositions[i]))
                    }
                }
            });
            $scope.allLeaves = Leave.query();
            $scope.allRecruitments = Recruitment.query();
            $scope.allAppliances = Appliance.query();
            $scope.perksLastMonth = false;
            $scope.currentLeaves = false;
            $scope.pastMonthLeaves = false;
            var getDaysOfMonth = function (date) {
                return new Date(date.getFullYear(), date.getMonth(), 0).getDate();
            }

            $scope.perksSum = function (worker) {
                var sum = 0;
                for (var i = 0; i < worker.perks.length; i++) {
                    var date = new Date(worker.perks[i].date);
                    if (date < $scope.newDate && date > $scope.monthBeforeDate) {
                        sum = sum + parseFloat(worker.perks[i].perks);
                    }
                }
                if (sum != 0) {
                    $scope.perksLastMonth = true;
                }
                return sum;
            }
            $scope.sumAllPerks = function () {
                var sumAll = 0
                for (var j = 0; j < $scope.allWorkers.length; j++) {
                    for (var i = 0; i < $scope.allWorkers[j].perks.length; i++) {
                        if (new Date($scope.allWorkers[j].perks[i].date) < $scope.newDate && new Date($scope.allWorkers[j].perks[i].date) > $scope.monthBeforeDate) {
                            sumAll = sumAll + parseFloat($scope.allWorkers[j].perks[i].perks);
                        }
                    }
                }
                return sumAll;
            };
            $scope.workersOnPosition = function (position) {
                var ammount = 0;
                for (var i = 0; i < $scope.allWorkers.length; i++) {
                    if ($scope.allWorkers[i].positions.length != 0) {
                        if ($scope.allWorkers[i].positions[0].id == position.id) {
                            ammount++;
                        }
                    }
                }
                return ammount;
            };
            $scope.perksSumAll = function (worker) {
                var sum = 0;
                for (var i = 0; i < worker.perks.length; i++) {
                    var date = new Date(worker.perks[i].date);
                    sum = sum + parseFloat(worker.perks[i].perks);
                }
                return sum;
            }
            $scope.sumAllPerksAll = function () {
                var sumAll = 0
                for (var j = 0; j < $scope.allWorkers.length; j++) {
                    for (var i = 0; i < $scope.allWorkers[j].perks.length; i++) {
                        sumAll = sumAll + parseFloat($scope.allWorkers[j].perks[i].perks);
                    }
                }
                return sumAll;
            }
            $scope.sumAllSalary = function () {
                var sum = 0;
                for (var i = 0; i < $scope.allWorkers.length; i++) {
                    if ($scope.allWorkers[i].positions != 0) {
                        sum = sum + parseFloat($scope.allWorkers[i].positions[0].salary);
                    }
                }
                return sum;
            }
            $scope.salarySummedForPosition = function (position) {
                var result = 0;
                if (position.workers != 0) {
                    result = (parseFloat(position.salary) * position.workers.length)
                }
                return result;
            }
            $scope.days = function (from, to) {
                var fromDate = new Date(from);
                var toDate = new Date(to);
                var fromDateDay = fromDate.getDate();
                var toDateDay = toDate.getDate();
                if (toDateDay < fromDateDay) {
                    return toDateDay - fromDateDay + getDaysOfMonth(toDate);
                } else
                    return toDateDay - fromDateDay;
            }
            $scope.countOpenRecruitments = function () {
                var count = 0;
                for (var i = 0; i < $scope.allRecruitments.length; i++) {
                    if ($scope.allRecruitments[i].status == true) {
                        count++;
                    }
                }
                return count;
            }
            $scope.length = $rootScope.currentUser.perks.length;
            $scope.filterPerksAll = function (worker) {
                return $scope.perksSumAll(worker) != 0;
            }
            $scope.filterPerks = function (worker) {
                return $scope.perksSum(worker) != 0;
            }
            $scope.filterEmptyPositions = function (position) {
                return position.workers.length != 0;
            }
            $scope.filterCurrentLeaves = function (leave) {
                var tmp = new Date(leave.from) <= $scope.today && $scope.today <= new Date(leave.to);
                if (tmp == true && leave.approved == true) {
                    $scope.currentLeaves = true;
                }
                return tmp == true && leave.approved == true;
            }
            $scope.filterLeavesFromPastMonth = function (leave) {
                var tmp = ((new Date(leave.from) || new Date(leave.to)) >= $scope.monthBeforeDate) && ((new Date(leave.from) || new Date(leave.to)) <= $scope.newDate);
                if (tmp == true) {
                    $scope.pastMonthLeaves = true;
                }
                return ((new Date(leave.from) || new Date(leave.to)) >= $scope.monthBeforeDate) && ((new Date(leave.from) || new Date(leave.to)) <= $scope.newDate);
            }
            $scope.filterOpenedRecruitments = function (recruitment) {
                return recruitment.status == true;
            }
            $scope.filterLeaveApproved = function (leave) {
                if (leave.approved != true) {
                    $scope.currentLeaves = false;
                }
                return leave.approved == true;
            }
        }
    }
]);