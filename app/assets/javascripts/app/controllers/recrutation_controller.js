'use strict';

this.appControllers.controller('RecruitmentController', [
    'Appliance', 'WorkerService', 'Recruitment', 'Position', 'Qualification', 'Worker', '$scope', '$stateParams', '$state', 'ngNotify', 'FileUploader', 'pdfDelegate', function (Appliance, WorkerService, Recruitment, Position, Qualification, Worker, $scope, $stateParams, $state, ngNotify, FileUploader, pdfDelegate) {
        this.init = function () {
            $scope.allRecruitments = Recruitment.query(function () {
                $scope.allRecruitments.filter(function (recruitment) {
                    recruitment.positionName = [];
                    recruitment.positionName = recruitment.position.name;
                });
            });
            $scope.availableSearchParams = [
                {key: "positionName", name: "Name", placeholder: "Name..."},
            ];
            $scope.isInternship = function (recruitment) {
                return recruitment.position.internship == true;
            };
            $scope.isJob = function (recruitment) {
                return recruitment.position.internship == false;
            };
        };
        this.openRecruitment = function (recruitments) {
            Recruitment.update({
                id: recruitments.id
            }, {recruitment: {status: true}}).$promise.then(function (position) {
                ngNotify.set('Reqruitment opened!', 'success');
                $scope.allRecruitments = Recruitment.query();
            });
        };
        this.closeRecruitment = function (recruitments) {
            Recruitment.update({
                id: recruitments.id
            }, {recruitment: {status: false}}).$promise.then(function (position) {
                ngNotify.set('Reqruitment closed!', 'error');
                $scope.allRecruitments = Recruitment.query();
            });
        };
        this.accept = function () {
            $scope.allQualifications = Qualification.query();
            $scope.allPositions = Position.query();
            $scope.remove = function(qualification) {
                $scope.newWorker.propositions_of_qualifications.splice($scope.newWorker.propositions_of_qualifications.indexOf(qualification), 1);
                ngNotify.set("Proposition removed", "error");
            };
            $scope.newWorker = Appliance.get({id: $stateParams.id});
            Appliance.get({id: $stateParams.id}, function (res) {
                $scope.newWorker.position_ids = [res.recruitment.position.id];
                pdfDelegate.$getByHandle('my-pdf-container').load(res.cv_url);

            });
        };
        this.createQualification = function(qualification){
            $scope.remove = function(qualification) {
                $scope.newWorker.propositions_of_qualifications.splice($scope.newWorker.propositions_of_qualifications.indexOf(qualification), 1);
            };
            Qualification.save({ qualification: qualification }).$promise.then(function (createdQualification) {
                ngNotify.set("Qualification created", 'success');
                $scope.remove(qualification);
                $scope.allQualifications.push(createdQualification);
                $scope.newWorker.qualification_ids.push(createdQualification.id);
            }, function (errors) {
                $scope.errors = errors.data;
                ngNotify.set("Name is already taken", "error");
                console.error($scope.errors);
            });
        }
        this.finishAccepting = function(isValid){
            if(isValid){
                WorkerService.createFromApliance($scope.newWorker, function(res){
                        ngNotify.set("Appliance accepted, worker created", 'success');
                }, function(failure) {
                    ngNotify.set("Error occured when trying to accept CV, contact administrator", "error");
                });
            }
        };
        this.cvToUpload = function () {
            $scope.cvToUpload = Recruitment.get({id: $stateParams.id});
            $scope.uploader = new FileUploader({
                url: '/api/attachments'
            });
            $scope.uploader.filters.push({
                name: 'pdfFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|pdf|'.indexOf(type) !== -1;
                }
            });
            $scope.uploader.queueLimit = 1;
            $scope.allQualifications = Qualification.query();
            $scope.newAppliance = {};
            $scope.newAppliance.recruitment_id = $stateParams.id;
            $scope.newAppliance.approved = false;
            $scope.newAppliance.propositions_of_qualifications = [{name: ""}];
            $scope.addNew = function() {
                $scope.newAppliance.propositions_of_qualifications.push({name: ""});
            };
            $scope.remove = function(qualification) {
                $scope.newAppliance.propositions_of_qualifications.splice($scope.newAppliance.propositions_of_qualifications.indexOf(qualification),1);
                ngNotify.set("Proposition removed", "error");
            };
            $scope.uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                ngNotify.set("You can add only 1 file, with .pdf extension", 'error');
            };
            $scope.uploader.onAfterAddingFile = function (fileItem) {
                ngNotify.set("You've added file correctly", 'success');
                $scope.newAppliance.cv = fileItem.url;
            };
            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                ngNotify.set("File uploded correctly", 'success');
                $scope.uploaded = true;
            };
            $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                ngNotify.set("There was problem with uploading your CV", 'error');

            };
        };
        this.upload = function (isValid) {
            if (isValid) {
                Appliance.save({appliance: $scope.newAppliance}).$promise.then(function (createdAppliance) {
                    $scope.cvToUpload = {};
                    ngNotify.set("Your CV has been sent", 'success');
                }, function (errors) {
                    $scope.errors = errors.data;
                    console.error($scope.errors);
                });
            }
        };
        this.showCV = function () {
            $scope.availableSearchParams = [
                {key: "first_name", name: "First name", placeholder: "First name..."},
                {key: "last_name", name: "Last name", placeholder: "Last name..."},
                {key: "created_at", name: "Uploaded on", placeholder: "yyyy-mm-dd..."},
            ];

            $scope.recruitment = Recruitment.get({id: $stateParams.id});
            $scope.allApliances = Appliance.query();
            $scope.recruitment_id = $stateParams.id;
            $scope.length = $scope.allApliances.length;
            $scope.currentRecruitment = function (appliance) {
                return appliance.recruitment_id == $stateParams.id;
            };
            $scope.notApproved = function (appliance) {
                return appliance.approved == false;
            };
        };
        this.details = function () {
            $scope.appliance = Appliance.get({id: $stateParams.id}, function () {
                pdfDelegate.$getByHandle('my-pdf-container').load($scope.appliance.cv_url);
            });
            $scope.allApliances = Appliance.query();
        };

        //pdf viewer events

        this.zoomInViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').zoomIn();
        };
        this.zoomOutViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').zoomOut();
        };
        this.nextPageViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').next();
        };
        this.previousPageViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').prev();
        }
    }
]);
