'use strict';

this.appControllers.controller('WorkingTimesController', [
    'WorkingTime', 'WorkerService', 'Worker', 'Position', 'uiCalendarConfig', '$scope', '$stateParams', 'ngNotify', '$state', '$compile', '$http', function (WorkingTime, WorkerService, Worker, Position, uiCalendarConfig, $scope, $stateParams, ngNotify, $state, $compile, $http) {
        this.initForm = function () {
            $scope.allPositions = Position.query();
            $scope.newWorkingTime = {};
            $scope.minTime = new Date(1970, 0, 1, 6, 0, 0);
            $scope.everyDay = false;
            $scope.everyWeek = false;
            $scope.times = 2;
            $scope.withoutWeeks = false;
        };
        this.createWorkingTime = function (isValid) {
            if (isValid) {
                $scope.date = new Date($scope.newWorkingTime.date);
                $scope.hours = $scope.date.getUTCHours() + 2;
                $scope.hoursFrom = $scope.newWorkingTime.from.getUTCHours() + 2;
                $scope.hoursTo = $scope.newWorkingTime.to.getUTCHours() + 2;
                if ($scope.everyDay) {
                    $scope.everyWeek = false;
                    var j = 0;
                    var k;
                    for (var i = 0; i < $scope.times; i++) {
                        if ($scope.withoutWeeks) {
                            k = i - j;
                            if ((($scope.date.getDay() + k) / 6) == (1 + j)) {
                                i = i + 2;
                                $scope.times = $scope.times + 2;
                                j++;
                                WorkingTime.save({
                                    working_time: {
                                        date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                        from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                        to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                        position_ids: $scope.newWorkingTime.position_ids
                                    }
                                }).$promise.then(function (createdWorkingTime) {
                                    ngNotify.set('Hours of work created', 'success');

                                }, function (errors) {
                                    $scope.errors = errors.data;
                                    console.error($scope.errors);
                                });
                            } else if (($scope.date.getDay() + k) == 0) {
                                i = i + 1;
                                $scope.times = $scope.times + 1;
                                WorkingTime.save({
                                    working_time: {
                                        date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                        from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                        to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                        position_ids: $scope.newWorkingTime.position_ids
                                    }
                                }).$promise.then(function (createdWorkingTime) {
                                    ngNotify.set('Hours of work created', 'success');
                                }, function (errors) {
                                    $scope.errors = errors.data;
                                    console.error($scope.errors);
                                });
                            } else {
                                WorkingTime.save({
                                    working_time: {
                                        date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                        from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                        to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                        position_ids: $scope.newWorkingTime.position_ids
                                    }
                                }).$promise.then(function (createdWorkingTime) {
                                    ngNotify.set('Hours of work created', 'success');
                                }, function (errors) {
                                    $scope.errors = errors.data;
                                    console.error($scope.errors);
                                });
                            }
                        } else {
                            WorkingTime.save({
                                working_time: {
                                    date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                    from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                    to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                    position_ids: $scope.newWorkingTime.position_ids
                                }
                            }).$promise.then(function (createdWorkingTime) {
                                ngNotify.set('Hours of work created', 'success');
                            }, function (errors) {
                                $scope.errors = errors.data;
                                console.error($scope.errors);
                            });
                        }

                    }
                } else if ($scope.everyWeek) {
                    $scope.everyday = false;
                    for (var i = 0; i < $scope.times * 7; i = i + 7) {
                        WorkingTime.save({
                            working_time: {
                                date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                position_ids: $scope.newWorkingTime.position_ids
                            }
                        }).$promise.then(function (createdWorkingTime) {
                            ngNotify.set('Hours of work created', 'success');
                        }, function (errors) {
                            $scope.errors = errors.data;
                            console.error($scope.errors);
                        });
                    }
                } else {
                    $scope.newWorkingTime.date = new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate(), $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds());
                    $scope.newWorkingTime.from = new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds());
                    $scope.newWorkingTime.to = new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds());
                    WorkingTime.save({working_time: $scope.newWorkingTime}).$promise.then(function (createdWorkingTime) {
                        ngNotify.set('Hours of work created', 'success');
                    }, function (errors) {
                        $scope.errors = errors.data;
                        console.error($scope.errors);
                    });
                }
            }
        };
        this.showWorkers = function () {
            $scope.allWorkers = Worker.query();
            $scope.position = Position.get({id: $stateParams.id});
            $scope.availableSearchParams = [
                {key: "first_name", name: "First name", placeholder: "First name..."},
                {key: "last_name", name: "Last name", placeholder: "Last name..."},
                {key: "pesel", name: "Pesel", placeholder: "Pesel..."},
                {key: "positionNames", name: "Position", placeholder: "Position..."},
            ];
            $scope.onPosition = function (worker) {
                return worker.positions[0].id == $scope.position.id;
            }
            $scope.length = $scope.allWorkers.length;
        }
        this.showWorkingTime = function () {
            $scope.listOfWorkingTimes = WorkingTime.query(function () {
                $scope.listOfWorkingTimes.filter(function (working_time) {
                    working_time.from = new Date(working_time.from);
                    working_time.to = new Date(working_time.to);
                    if (working_time.to < working_time.from) {
                        working_time.to.setDate(working_time.to.getDate() + 1);
                    }
                    var msec = working_time.to - working_time.from;
                    working_time.how_much_to_work = {};
                    working_time.how_much_to_work.hours = Math.floor(msec / 1000 / 60 / 60);
                    msec -= working_time.how_much_to_work.hours * 1000 * 60 * 60;
                    working_time.how_much_to_work.minutes = Math.floor(msec / 1000 / 60);
                    msec -= working_time.how_much_to_work.minutes * 1000 * 60;
                    working_time.how_much_to_work.seconds = Math.floor(msec / 1000);
                    working_time.positionNames = [];
                    working_time.positions.filter(function (position) {
                        working_time.positionNames.push(position.name)
                    });
                });
            });

            $scope.today = moment().format('YYYY-MM-DD');
            $scope.filterDates = function (working_Time) {
                return working_Time.date > $scope.today;
            };
            $scope.availableSearchParams = [
                {key: "date", name: "Date", placeholder: "yyyy-mm-dd..."},
                {key: "from", name: "Starting time", placeholder: "hh:mm..."},
                {key: "to", name: "Ending time", placeholder: "hh:mm..."},
                {key: "positionNames", name: "Position", placeholder: "Name..."},
            ];
        };
        this.schedule = function () {
            $scope.events = [];
            $scope.worker = Worker.get({id: $stateParams.id});
            $scope.uiConfig = {
                calendar: {
                    timeFormat: 'H(:mm)',
                    editable: false,
                    firstDay: 1,
                    allDaySlot: true,
                    axisFormat: 'H:mm',
                    defaultView: 'agendaWeek',
                    header: {
                        left: 'month agendaWeek agendaDay',
                        center: 'title',
                        right: 'today prev,next'
                    },
                }
            };
            WorkerService.getWorkingTimesForUser($stateParams.id, function (res) {
                res.filter(function (item) {
                    var show = {};
                    var itemFrom = new Date(new Date(item.date).getFullYear(), new Date(item.date).getMonth(), new Date(item.date).getDate(), new Date(item.from).getHours(), new Date(item.from).getMinutes());
                    var itemTo = new Date(new Date(item.date).getFullYear(), new Date(item.date).getMonth(), new Date(item.date).getDate(), new Date(item.to).getHours(), new Date(item.to).getMinutes());
                    if (item.positions) {
                        show = {title: item.positions[0].name, start: itemFrom, end: itemTo, stick: true};
                        $scope.events.push(show);

                    } else if (item.approved == true) {
                        show = {
                            title: item.name,
                            start: new Date(item.from),
                            end: new Date(new Date(item.to).getFullYear(), new Date(item.to).getMonth(), new Date(item.to).getDate() + 1 , new Date(item.to).getHours(), new Date(item.to).getMinutes(), new Date(item.to).getSeconds()),
                            stick: true,
                            allDay: true,
                            color: "#2C0811"
                        };
                        $scope.events.push(show);
                    }
                });
            })
            $scope.eventSources = [$scope.events];
        }
        this.showWorkingTimeForWorker = function () {
            $scope.events = [];
            $scope.uiConfig = {
                calendar: {
                    timeFormat: 'H(:mm)',
                    editable: false,
                    firstDay: 1,
                    allDaySlot: true,
                    axisFormat: 'H:mm',
                    defaultView: 'agendaWeek',
                    header: {
                        left: 'month agendaWeek agendaDay',
                        center: 'title',
                        right: 'today prev,next'
                    },
                }
            };
            WorkerService.getWorkingTimesForWorker(function (res) {
                res.filter(function (item) {
                    var show = {};
                    var itemFrom = new Date(new Date(item.date).getFullYear(), new Date(item.date).getMonth(), new Date(item.date).getDate(), new Date(item.from).getHours(), new Date(item.from).getMinutes());
                    var itemTo = new Date(new Date(item.date).getFullYear(), new Date(item.date).getMonth(), new Date(item.date).getDate(), new Date(item.to).getHours(), new Date(item.to).getMinutes());
                    if (item.positions) {
                        show = {title: item.positions[0].name, start: itemFrom, end: itemTo, stick: true};
                        $scope.events.push(show);

                    } else if (item.approved == true) {
                        show = {
                            title: item.name,
                            start: new Date(item.from),
                            end: new Date(new Date(item.to).getFullYear(), new Date(item.to).getMonth(), new Date(item.to).getDate() + 1 , new Date(item.to).getHours(), new Date(item.to).getMinutes(), new Date(item.to).getSeconds()),
                            stick: true,
                            allDay: true,
                            color: "#2C0811"
                        };
                        $scope.events.push(show);
                    }
                });
            })
            $scope.eventSources = [$scope.events];
            $scope.listOfWorkingTimesForWorker = [];
            WorkerService.getWorkingTimesForWorker(function (res) {
                $scope.listOfWorkingTimesForWorker = res;
                $scope.listOfWorkingTimesForWorker.filter(function (working_time) {
                    working_time.from = new Date(working_time.from);
                    working_time.to = new Date(working_time.to);
                    if (working_time.to < working_time.from) {
                        working_time.to.setDate(working_time.to.getDate() + 1);
                    }
                    var msec = working_time.to - working_time.from
                    working_time.how_much_to_work = {};
                    working_time.how_much_to_work.hours = Math.floor(msec / 1000 / 60 / 60);
                    msec -= working_time.how_much_to_work.hours * 1000 * 60 * 60;
                    working_time.how_much_to_work.minutes = Math.floor(msec / 1000 / 60);
                    msec -= working_time.how_much_to_work.minutes * 1000 * 60;
                    working_time.how_much_to_work.seconds = Math.floor(msec / 1000)
                });
            });
            $scope.today = moment().format('YYYY-MM-DD');
            $scope.todayDay = moment().format('dddd');
        };
        this.workingTimeToEdit = function () {
            $scope.workingTimeToEdit = WorkingTime.get({id: $stateParams.id}, function () {
                $scope.workingTimeToEdit.from = new Date($scope.workingTimeToEdit.from);
                $scope.workingTimeToEdit.to = new Date($scope.workingTimeToEdit.to);
            });
            $scope.allPositions = Position.query();
        };
        this.deleteWorkingTime = function (working_time) {
            working_time.$remove (function () {
                $scope.listOfWorkingTimes.splice($scope.listOfWorkingTimes.indexOf(working_time), 1)
                ngNotify.set('Hours of work deleted', 'error');
            });
        };
        this.editWorkingTime = function (isValid) {
            if (isValid) {
                WorkingTime.update({
                    id: $scope.workingTimeToEdit.id
                }, {working_time: $scope.workingTimeToEdit}).$promise.then(function (working_time) {
                    $scope.workingTimeForm.$setPristine();
                    $state.go('showHoW');
                    ngNotify.set('Hours of work saved', 'success');
                });
            }
        };

        // Calendar
        $scope.disabled = function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 0);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events =
            [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];
        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date);
                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date);
                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }
            return '';
        };
    }
])
;