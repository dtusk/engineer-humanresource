'use strict';

this.appControllers.controller('PerkController', [
    'Worker', 'Perk', '$scope', '$stateParams', '$state', 'ngNotify', function (Worker, Perk, $scope, $stateParams, $state, ngNotify) {
        this.init = function () {
            $scope.allWorkers = Worker.query();
            $scope.newPerk = {};
        };
        this.createPerk = function (isValid) {
            if (isValid) {
                Perk.save({perk: $scope.newPerk}).$promise.then(function (createdPerk) {
                    $scope.newPerk = {};
                    ngNotify.set("You've created new perk", "success");
                }, function (errors) {
                    $scope.errors = errors.data;
                    console.error($scope.errors);
                });
            }
        };
        this.showPerks = function () {
            $scope.allWorkers = Worker.query();
            $scope.availableSearchParams = [
                {key: "date", name: "Date", placeholder: "yyyy-mm-dd..."},
                {key: "reason", name: "Reason", placeholder: "Reason..."},
                {key: "perks", name: "Ammout", placeholder: "Number..."},
                {key: "worker_first_name", name: "Worker first name", placeholder: "Firstname..."},
                {key: "worker_last_name", name: "Worker last name", placeholder: "Lastname..."},
            ];
            $scope.listOfPerks = Perk.query(function () {
                $scope.length = $scope.listOfPerks.length;
                $scope.listOfPerks.filter(function (perk) {
                    perk.worker_first_name = perk.worker.first_name;
                    perk.worker_last_name = perk.worker.last_name;
                });
            });
        };
    }
]);
