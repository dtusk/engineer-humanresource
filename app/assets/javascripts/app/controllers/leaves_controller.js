'use strict';

this.appControllers.controller('LeavesController', [
    'Leave', 'Worker', 'uiCalendarConfig', '$scope', '$rootScope', '$stateParams', 'ngNotify', '$state', 'FileUploader', 'pdfDelegate', function (Leave, Worker, uiCalendarConfig, $scope, $rootScope, $stateParams, ngNotify, $state, FileUploader, pdfDelegate) {
        this.init = function () {
            $scope.uploaded = false;
            $scope.newRequest = {};
            $scope.uploader = new FileUploader({
                url: '/api/attachments'
            });
            $scope.uploader.filters.push({
                name: 'pdfFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|pdf|'.indexOf(type) !== -1;
                }
            });
            $scope.uploader.queueLimit = 1;
            $scope.uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                ngNotify.set("You can add only 1 file with extension .pdf", 'error');
            };
            $scope.uploader.onAfterAddingFile = function (fileItem) {
                ngNotify.set("You've added file correctly", 'success');
                $scope.newRequest.attachment = fileItem.url;
            };
            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                ngNotify.set("File uploded correctly", 'success');
                $scope.uploaded = true;
            };
            $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                ngNotify.set("There was problem with uploading your CV", 'error');

            };

        };

        this.createLeave = function (isValid) {
            if (isValid) {
                var hoursFrom = $scope.newRequest.from.getHours() + 1;
                var hoursTo = $scope.newRequest.to.getHours() + 1;
                $scope.newRequest.to = new Date($scope.newRequest.to.getFullYear(), $scope.newRequest.to.getMonth(), $scope.newRequest.to.getDate(), hoursFrom, $scope.newRequest.to.getMinutes(), $scope.newRequest.to.getSeconds());
                $scope.newRequest.from = new Date($scope.newRequest.from.getFullYear(), $scope.newRequest.from.getMonth(), $scope.newRequest.from.getDate(), hoursTo, $scope.newRequest.from.getMinutes(), $scope.newRequest.from.getSeconds())
                $scope.newRequest.worker_id = $rootScope.currentUser.id;
                Leave.save({leave: $scope.newRequest}).$promise.then(function (createdLeave) {
                    console.log(createdLeave);
                    ngNotify.set("Your request has been sent", 'success');
                }, function (errors) {
                    $scope.error = errors.data;
                    console.error($scope.error);
                });
            }
        };
        this.showLeaves = function () {
            $scope.allWorkers = Worker.query();
            $scope.pendingRequests = false;
            $scope.seenRequests = false;
            $scope.availableSearchParams = [
                {key: "name", name: "Name", placeholder: "Name..."},
                {key: "from", name: "Date from", placeholder: "yyyy-mm-dd..."},
                {key: "to", name: "Date to", placeholder: "yyyy-mm-dd..."},
                {key: "worker_first_name", name: "Worker first name", placeholder: "First name..."},
                {key: "worker_last_name", name: "Worker last name", placeholder: "Last name..."},
                {key: "approved", name: "Approved", placeholder: "true(yes)/false(no)/!(not yet)"}
            ];
            $scope.allLeaves = Leave.query(function () {
                $scope.pendingRequests = false;
                $scope.seenRequests = false;
                $scope.length = $scope.allLeaves.length;
                $scope.allLeaves.filter(function (leave) {
                    if (leave.approved == null) {
                        $scope.pendingRequests = true;
                    }
                    if (leave.approved != null) {
                        $scope.seenRequests = true;
                    }
                    leave.worker_first_name = leave.worker.first_name;
                    leave.worker_last_name = leave.worker.last_name;
                });
            });
            $scope.showNotSeen = function (request) {
                return request.approved != null;
            }
            $scope.showSeen = function (request) {
                return request.approved == null;
            };

        };
        this.showOwn = function () {
            $scope.allLeaves = Leave.query(function () {
                $scope.length = $scope.allLeaves.length;
                $scope.allLeaves.filter(function (leave) {
                    leave.worker_first_name = leave.worker.first_name;
                    leave.worker_last_name = leave.worker.last_name;
                });
            });
            $scope.filterCurrentUser = function (workerLeave) {
                return (workerLeave.worker_id == $rootScope.currentUser.id);
            };
            $scope.availableSearchParams = [
                {key: "name", name: "Name", placeholder: "Name..."},
                {key: "from", name: "Date from", placeholder: "yyyy-mm-dd..."},
                {key: "to", name: "Date to", placeholder: "yyyy-mm-dd..."},
                {key: "approved", name: "Approved", placeholder: "true(yes)/false(no)/!(not yet)"}
            ];
        }
        this.showWorker = function () {
            $scope.worker = Worker.get({id: $stateParams.id});
        }
        this.showDetails = function () {
            $scope.leave = Leave.get({id: $stateParams.id}, function () {
                pdfDelegate.$getByHandle('my-pdf-container').load($scope.leave.file_url);
            });
        };
        this.approveRequest = function (request_id) {
            Leave.update({
                id: request_id
            }, {leave: {approved: true}}).$promise.then(function (leave) {
                ngNotify.set("Request for leave approved", "success");
                $scope.allLeaves = Leave.query(function () {
                    $scope.length = $scope.allLeaves.length;
                    $scope.allLeaves.filter(function (leave) {
                        if (leave.approved == null) {
                            $scope.pendingRequests = true;
                        }
                        if (leave.approved != null) {
                            $scope.seenRequests = true;
                        }
                        leave.worker_first_name = leave.worker.first_name;
                        leave.worker_last_name = leave.worker.last_name;
                    });
                });
            })
        };
        this.declineRequest = function (request_id) {
            Leave.update({
                id: request_id
            }, {leave: {approved: false}}).$promise.then(function (leave) {
                ngNotify.set("Request for leave declined", "error");
                $scope.allLeaves = Leave.query(function () {
                    $scope.length = $scope.allLeaves.length;
                    $scope.allLeaves.filter(function (leave) {
                        if (leave.approved == null) {
                            $scope.pendingRequests = true;
                        }
                        if (leave.approved != null) {
                            $scope.seenRequests = true;
                        }
                        leave.worker_first_name = leave.worker.first_name;
                        leave.worker_last_name = leave.worker.last_name;
                    });
                });
            })
        };

        // Calendar
        $scope.disabled = function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 0);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events =
            [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];
        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date);
                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date);
                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }
            return '';
        };

        $scope.filterNotArchivedWorker = function (leave) {
            return !leave.worker.archive_worker;
        }
        //pdf viewer events

        this.zoomInViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').zoomIn();
        };
        this.zoomOutViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').zoomOut();
        };
        this.nextPageViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').next();
        };
        this.previousPageViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').prev();
        }
    }
]);
