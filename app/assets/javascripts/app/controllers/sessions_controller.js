'use strict';

this.appControllers.controller('SessionsController', ['$scope', '$rootScope', '$state', 'ngNotify', 'Auth', '$interval', 'WorkerService', 'Worker',
    function ($scope, $rootScope, $state, ngNotify, Auth, $interval, WorkerService, Worker) {
        this.doLogin = function (isValid) {
            if (isValid) {
                $rootScope.sessionRoot = $scope.sessionForm;
                Auth.login($scope.sessionForm).then(function (user) {
                    $rootScope.isAuthenticated = Auth.isAuthenticated();
                    $state.go('main');
                    $rootScope.currentUser = Auth._currentUser
                    $rootScope.ifHR = Auth._currentUser.hr;
                    $rootScope.ifBoss = Auth._currentUser.boss;
                    $rootScope.ifAdmin = Auth._currentUser.admin;
                    $rootScope.ifWorker = Auth._currentUser.worker;
                    ngNotify.set('You are logged in', 'success');
                    //window.intervalIsLoggedIn = setInterval(window.sessionIsLoggedIn, 61000);
                    window.intervalIsLoggedIn = setInterval(window.sessionIsLoggedIn, 901000);
                }, function (error) {
                    $rootScope.isAuthenticated = Auth.isAuthenticated();
                    ngNotify.set("You've entered either wrong pesel or what is more likely to occur, password", 'error');
                });
            }
        }

        this.doRemind = function (worker_pesel) {
            WorkerService.remindPassword(worker_pesel, function (res) {
                ngNotify.set("We have sent you new password", "success");
            });
        };
    }]);
