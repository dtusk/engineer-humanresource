'use strict';

this.appControllers.controller('QualificationsController', [
    'Qualification', '$scope', '$stateParams', '$state', 'ngNotify', function (Qualification, $scope, $stateParams, $state, ngNotify) {
        this.createQualification = function (isValid) {
            if (isValid) {
                Qualification.save({ qualification: $scope.newQualification }).$promise.then(function (createdQualification) {
                    $scope.newQualification = { };
                    ngNotify.set("Qualification created", 'success')
                }, function (errors) {
                    $scope.errors = errors.data;
                    console.error($scope.errors);
                });
            }
        };
        this.showQualifications = function () {
            $scope.listOfQualifications = Qualification.query(function () {
                $scope.length = $scope.listOfQualifications.length;
            });
            $scope.availableSearchParams = [
                {key: "name", name: "Qualification", placeholder: "Name..."},
            ];
        };
        this.qualificationToEdit = function () {
            $scope.qualificationToEdit = Qualification.get({id: $stateParams.id});
        };
        this.deleteQualification = function (qualification) {
            qualification.$remove (function () {
                $scope.listOfQualifications.splice($scope.listOfQualifications.indexOf(qualification), 1);
                ngNotify.set("Qualification has been deleted", 'error');
            });
        };
        this.editQualification = function (isValid) {
            if (isValid) {
                Qualification.update({
                    id: $scope.qualificationToEdit.id
                }, {qualification: $scope.qualificationToEdit}).$promise.then(function (qualification) {
                    $scope.qualificationForm.$setPristine();
                    $state.go('showQualifications');
                    ngNotify.set('Qualification saved!', 'success');
                });
            }
        };
    }
]);