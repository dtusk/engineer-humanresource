'use strict';

this.appControllers.controller('WizardPositionController', [
    'Worker', 'Position', 'Qualification', 'WorkingTime', '$scope', '$rootScope', '$stateParams', '$state', 'ngNotify', function (Worker, Position, Qualification, WorkingTime, $scope, $rootScope, $stateParams, $state, ngNotify) {
        this.initScopes = function () {
            $rootScope.positionCreated = false;
            $rootScope.qualificationCreated = false;
            $rootScope.qualificationAdded = false;
            $rootScope.addedToWorker = false;
            $rootScope.workingTimeCreated = false;
        };
        this.init = function () {
            $scope.allQualifications = Qualification.query();
            $scope.newPosition = {};
        };
        this.initForm = function () {
            $scope.allPositions = Position.query();
            $scope.newWorkingTime = {};
        };
        this.createWorkingTime = function (isValid) {
            $rootScope.workingTimeCreated = true;
            if (isValid) {
                $scope.date = new Date($scope.newWorkingTime.date);
                $scope.hours = $scope.date.getUTCHours() + 2;
                $scope.hoursFrom = $scope.newWorkingTime.from.getUTCHours() + 2;
                $scope.hoursTo = $scope.newWorkingTime.to.getUTCHours() + 2;
                if ($scope.everyDay) {
                    $scope.everyWeek = false;
                    var j = 0;
                    var k;
                    for (var i = 0; i < $scope.times; i++) {
                        if ($scope.withoutWeeks) {
                            k = i - j;
                            if ((($scope.date.getDay() + k) / 6) == (1 + j)) {
                                i = i + 2;
                                $scope.times = $scope.times + 2;
                                j++;
                                WorkingTime.save({
                                    working_time: {
                                        date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                        from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                        to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                        position_ids: $scope.newWorkingTime.position_ids
                                    }
                                }).$promise.then(function (createdWorkingTime) {
                                    ngNotify.set('Hours of work created', 'success');

                                }, function (errors) {
                                    $scope.errors = errors.data;
                                    console.error($scope.errors);
                                });
                            } else if (($scope.date.getDay() + k) == 0) {
                                i = i + 1;
                                $scope.times = $scope.times + 1;
                                WorkingTime.save({
                                    working_time: {
                                        date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                        from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                        to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                        position_ids: $scope.newWorkingTime.position_ids
                                    }
                                }).$promise.then(function (createdWorkingTime) {
                                    ngNotify.set('Hours of work created', 'success');
                                }, function (errors) {
                                    $scope.errors = errors.data;
                                    console.error($scope.errors);
                                });
                            } else {
                                WorkingTime.save({
                                    working_time: {
                                        date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                        from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                        to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                        position_ids: $scope.newWorkingTime.position_ids
                                    }
                                }).$promise.then(function (createdWorkingTime) {
                                    ngNotify.set('Hours of work created', 'success');
                                }, function (errors) {
                                    $scope.errors = errors.data;
                                    console.error($scope.errors);
                                });
                            }
                        } else {
                            WorkingTime.save({
                                working_time: {
                                    date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                    from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                    to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                    position_ids: $scope.newWorkingTime.position_ids
                                }
                            }).$promise.then(function (createdWorkingTime) {
                                ngNotify.set('Hours of work created', 'success');
                            }, function (errors) {
                                $scope.errors = errors.data;
                                console.error($scope.errors);
                            });
                        }

                    }
                } else if ($scope.everyWeek) {
                    $scope.everyday = false;
                    for (var i = 0; i < $scope.times * 7; i = i + 7) {
                        WorkingTime.save({
                            working_time: {
                                date: new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate() + i, $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds()),
                                from: new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds()),
                                to: new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds()),
                                position_ids: $scope.newWorkingTime.position_ids
                            }
                        }).$promise.then(function (createdWorkingTime) {
                            ngNotify.set('Hours of work created', 'success');
                        }, function (errors) {
                            $scope.errors = errors.data;
                            console.error($scope.errors);
                        });
                    }
                } else {
                    $scope.newWorkingTime.date = new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate(), $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds());
                    $scope.newWorkingTime.from = new Date($scope.newWorkingTime.from.getUTCFullYear(), $scope.newWorkingTime.from.getUTCMonth(), $scope.newWorkingTime.from.getUTCDate(), $scope.hoursFrom, $scope.newWorkingTime.from.getUTCMinutes(), $scope.newWorkingTime.from.getUTCSeconds());
                    $scope.newWorkingTime.to = new Date($scope.newWorkingTime.to.getUTCFullYear(), $scope.newWorkingTime.to.getUTCMonth(), $scope.newWorkingTime.to.getUTCDate(), $scope.hoursTo, $scope.newWorkingTime.to.getUTCMinutes(), $scope.newWorkingTime.to.getUTCSeconds());
                    WorkingTime.save({working_time: $scope.newWorkingTime}).$promise.then(function (createdWorkingTime) {
                        ngNotify.set('Hours of work created', 'success');
                    }, function (errors) {
                        $scope.errors = errors.data;
                        console.error($scope.errors);
                    });
                }
            }
        };
        this.createPosition = function (isValid) {
            if (isValid) {
                $rootScope.positionCreated = true;
                if ($scope.newPosition.internship == true) {
                    var tmp = $scope.newPosition
                    Position.save({
                        position: {
                            name: tmp.name + " - internship",
                            salary: tmp.salary,
                            internship: tmp.internship,
                            qualification_ids: tmp.qualification_ids
                        }
                    }).$promise.then(function (createdPosition) {
                        ngNotify.set('Position created!', 'success');
                        $scope.newPosition = {};
                    }, function (errors) {
                        $scope.errors = errors.data;
                        console.error($scope.errors);
                    });
                } else if ($scope.newPosition.internship != true) {
                    Position.save({position: $scope.newPosition}).$promise.then(function (createdPosition) {
                        ngNotify.set('Position created!', 'success');
                        $scope.newPosition = {};
                    }, function (errors) {
                        $scope.errors = errors.data;
                        console.error($scope.errors);
                    });
                }
                ;
            }
            ;

        };
        this.createQualification = function (isValid) {
            Qualification.save({qualification: $scope.newQualification}).$promise.then(function (createdQualification) {
                $rootScope.qualificationCreated = true;
                $scope.newQualification = {};
                $scope.newQualificationForm.$setPristine();
                ngNotify.set("You've created new qualification", "success");
            }, function (errors) {
                $scope.errors = errors.data;
                console.error($scope.errors);
            })
        };
        this.showWorkers = function () {
            $scope.listOfWorkers = Worker.query(function () {
                $scope.listOfWorkers.filter(function (worker) {
                    worker.positionNames = [];
                    worker.positions.filter(function (position) {
                        worker.positionNames.push(position.name)
                    });
                });
            });
            $scope.filterNotArchivedWorker = function (worker) {
                return !worker.archive_worker;
            }
            $scope.length = $scope.listOfWorkers.length;
            $scope.availableSearchParams = [
                {key: "first_name", name: "First name", placeholder: "First name..."},
                {key: "last_name", name: "Last name", placeholder: "Last name..."},
                {key: "pesel", name: "Pesel", placeholder: "Pesel..."},
                {key: "positionNames", name: "Position", placeholder: "Position..."},
            ];
        };
        this.showPositions = function () {
            $scope.listOfPositions = Position.query();
            $scope.length = $scope.listOfPositions.length;
            $scope.availableSearchParams = [
                {key: "name", name: "Name", placeholder: "Name..."},
                {key: "salary", name: "Salary", placeholder: "Number..."},
                {key: "internship", name: "Internship", placeholder: "true/false"},
            ];
        };
        this.showWorkingTime = function () {
            $scope.listOfWorkingTimes = WorkingTime.query(function () {
                $scope.listOfWorkingTimes.filter(function (working_time) {
                    working_time.from = new Date(working_time.from);
                    working_time.to = new Date(working_time.to);
                    if (working_time.to < working_time.from) {
                        working_time.to.setDate(working_time.to.getDate() + 1);
                    }
                    var msec = working_time.to - working_time.from;
                    working_time.how_much_to_work = {};
                    working_time.how_much_to_work.hours = Math.floor(msec / 1000 / 60 / 60);
                    msec -= working_time.how_much_to_work.hours * 1000 * 60 * 60;
                    working_time.how_much_to_work.minutes = Math.floor(msec / 1000 / 60);
                    msec -= working_time.how_much_to_work.minutes * 1000 * 60;
                    working_time.how_much_to_work.seconds = Math.floor(msec / 1000);
                });
            });
            $scope.today = moment().format('YYYY-MM-DD');
        };
        this.positionToEdit = function () {
            $scope.positionToEdit = Position.get({id: $stateParams.id});
            $scope.allQualifications = Qualification.query();
            $scope.omitSelectedQualifications = function (qualifications) {
                return $scope.allQualifications.indexOf(qualifications) === -1;
            };
        };
        this.editPosition = function (isValid) {
            $rootScope.qualificationAdded = true;
            if (isValid) {
                Position.update({
                    id: $scope.positionToEdit.id
                }, {position: $scope.positionToEdit}).$promise.then(function (position) {
                    ngNotify.set("You've edited position", 'success');
                });
            }
        };
        this.workerToEdit = function () {
            $scope.workerToEdit = Worker.get({id: $stateParams.id});
            $scope.allPositions = Position.query();
            $scope.allQualifications = Qualification.query();
        };
        this.editWorker = function (isValid) {
            $rootScope.addedToWorker = true;
            if (isValid) {
                Worker.update({
                    id: $scope.workerToEdit.id
                }, {worker: $scope.workerToEdit}).$promise.then(function (worker) {
                    ngNotify.set("You've edited worker", 'success');
                });
            }
        };
        //calendar
        $scope.disabled = function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 0);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events =
            [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];
        $scope.getDayClass = function (date, mode) {
            if (mode === 'day') {
                var dayToCheck = new Date(date);
                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date);
                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }
            return '';
        };
    }
]);