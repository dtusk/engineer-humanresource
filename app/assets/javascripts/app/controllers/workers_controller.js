'use strict';

this.appControllers.controller('WorkersController', [
    'Worker', 'Perk', 'Position', 'Qualification', '$scope', '$rootScope', '$stateParams', '$state', 'ngNotify', 'FileUploader', 'pdfDelegate', function (Worker, Perk, Position, Qualification, $scope, $rootScope, $stateParams, $state, ngNotify, FileUploader, pdfDelegate) {
        this.init = function () {
            $scope.allPositions = Position.query();
            $scope.allQualifications = Qualification.query();
            $scope.newWorker = {};
            $scope.uploader = new FileUploader({
                url: '/api/attachments'
            });
            $scope.uploader.filters.push({
                name: 'pdfFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|pdf|'.indexOf(type) !== -1;
                }
            });
            $scope.uploader.queueLimit = 1;
            $scope.uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                ngNotify.set("You can add only 1 file, with .pdf extension", 'error');
            };
            $scope.uploader.onAfterAddingFile = function (fileItem) {
                ngNotify.set("You've added file correctly", 'success');
                $scope.newWorker.cv = fileItem.url;
            };
            $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                ngNotify.set("File uploded correctly", 'success');
                $scope.uploaded = true;
            };
            $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
                ngNotify.set("There was problem with uploading your CV", 'error');
            };
        };
        this.statistics = function () {
            $scope.todayMoment = moment().format('YYYY-MM-DD');
            $scope.worker = Worker.get({id: $stateParams.id}, function () {
                $scope.salary = function () {
                    var result = 0;
                    if (new Date($scope.worker.created_at) > new Date(year, month + 1, 0)) {
                        result = parseFloat($scope.worker.positions[0].salary);
                    } else result = 0;
                    return result;
                }
                $scope.getSum = function () {
                    var sum = 0;
                    for (var i = 0; i < $scope.worker.perks.length; i++) {
                        var perk = parseFloat($scope.worker.perks[i].perks);
                        sum = perk + sum;
                    }
                    ;
                    return sum;
                };
                $scope.getSumFromLastMonth = function () {
                    var sum = 0;
                    for (var i = 0; i < $scope.worker.perks.length; i++) {
                        var tmp = new Date($scope.worker.perks[i].date);
                        if (tmp < $scope.newDate && tmp > $scope.monthBeforeDate) {
                            var perk = parseFloat($scope.worker.perks[i].perks);
                            sum = perk + sum;
                        }
                    }
                    ;
                    $scope.sum = $scope.salary() + sum;
                    return sum;
                };
            });
            $scope.today = new Date();
            var month = $scope.today.getMonth();
            var year = $scope.today.getFullYear();
            $scope.newDate = new Date(year, month, 1);
            $scope.monthBeforeDate = new Date(year, month - 1, $scope.newDate.getDate());
            var getDaysOfMonth = function (date) {
                return new Date(date.getFullYear(), date.getMonth(), 0).getDate();
            }
            $scope.days = function (from, to) {
                var fromDate = new Date(from);
                var toDate = new Date(to);
                var fromDateDay = fromDate.getDate();
                var toDateDay = toDate.getDate();
                if (toDateDay < fromDateDay) {
                    return toDateDay - fromDateDay + getDaysOfMonth(toDate);
                } else
                    return toDateDay - fromDateDay;
            }
            $scope.filterDates = function (leave) {
                return leave.date < $scope.newDate && leave.date > $scope.monthBeforeDate;
            }
            $scope.filterApproved = function (leave) {
                return leave.approved == true;
            }

        };
        this.createWorker = function (isValid) {
            if (isValid) {
                Worker.save({worker: $scope.newWorker}).$promise.then(function (createdWorker) {
                    $scope.newWorker = {};
                    ngNotify.set("You've created new worker", "success");
                    //$scope.newWorkerForm.$setPristine();
                }, function (errors) {
                    $scope.errors = errors.data;
                    console.error($scope.errors);
                });
            }
        };
        this.showWorkers = function () {
            $scope.availableSearchParams = [
                {key: "first_name", name: "First name", placeholder: "First name..."},
                {key: "last_name", name: "Last name", placeholder: "Last name..."},
                {key: "pesel", name: "Pesel", placeholder: "Pesel..."},
                {key: "positionNames", name: "Position", placeholder: "Position..."},
            ];
            $scope.listOfWorkers = Worker.query(function () {
                $scope.listOfWorkers.filter(function (worker) {
                    worker.positionNames = [];
                    worker.positions.filter(function (position) {
                        worker.positionNames.push(position.name)
                    });
                });
            });
            $scope.required_qualifications = function (worker) {
                var match = false;
                if (worker.positions.length > 0) {
                    if (worker.positions[0].qualifications.length > 0) {
                        if (worker.qualifications.length > 0) {
                            for (var i = 0; i < worker.positions[0].qualifications.length; i++) {
                                for (var j = 0; j < worker.qualifications.length; j++) {
                                    if (worker.positions[0].qualifications.length > worker.qualifications.length) {
                                        match = false;
                                    }
                                    if (worker.positions[0].qualifications.length == worker.qualifications.length || worker.positions[0].qualifications.length < worker.qualifications.length) {
                                        if (worker.positions[0].qualifications[i].id == worker.qualifications[j].id) {
                                            match = true;
                                            break;
                                        } else {
                                            match = false;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        match = true;
                    }
                }
                return match;
            }

        };
        this.initAddPerks = function () {
            $scope.workerToAddPerks = Worker.get({id: $stateParams.id});
            $scope.newPerks = {};
            $scope.date = new Date()
            $scope.hours = $scope.date.getUTCHours() + 2;
            $scope.newPerks.date = new Date($scope.date.getUTCFullYear(), $scope.date.getUTCMonth(), $scope.date.getUTCDate(), $scope.hours, $scope.date.getUTCMinutes(), $scope.date.getUTCSeconds())
            $scope.newPerks.worker_id = $stateParams.id;
            $scope.newPerks.worker = $scope.workerToAddPerks;
        };
        this.addPerks = function (isValid) {
            if (isValid) {
                Perk.save({perk: $scope.newPerks}).$promise.then(function (createdPerks) {
                    $scope.newPerks = {};
                    ngNotify.set("Perks added to worker", 'success');
                }, function (errors) {
                    $scope.errors = errors.data;
                    console.error($scope.errors);
                });
            }
            ;
        };
        this.workerToEdit = function () {
            $scope.workerToEdit = Worker.get({id: $stateParams.id});
            $scope.allPositions = Position.query();
            $scope.allQualifications = Qualification.query();
        };
        this.workerToShow = function () {
            $scope.workerToShow = Worker.get({id: $stateParams.id}, function () {
                if ($scope.workerToShow.cv_url != null)
                    pdfDelegate.$getByHandle('my-pdf-container').load($scope.workerToShow.cv_url);
            });

        };
        this.archiveWorker = function (worker) {
            Worker.update({
                id: worker.id
            }, {worker: {archive_worker: true}}).$promise.then(function (worker) {
                ngNotify.set("You've archived worker", 'error');
                $state.go('showArchive');
            });
        };
        this.showArchives = function(){
            $scope.availableSearchParams = [
                {key: "first_name", name: "First name", placeholder: "First name..."},
                {key: "last_name", name: "Last name", placeholder: "Last name..."},
                {key: "pesel", name: "Pesel", placeholder: "Pesel..."},
                {key: "updated_at", name: "Fired", placeholder: "yyyy-MM-dd..."},
                {key: "created_at", name: "Hired", placeholder: "yyyy-MM-dd..."},
            ];
            $scope.listOfArchived = Worker.query();
        };
        this.archivedWorkerDetails = function(){
            $scope.archivedWorker = Worker.get({id: $stateParams.id});
        }
        this.hire = function(worker_id){
            Worker.update({
                id: worker_id
            }, {worker: {archive_worker: false}}).$promise.then(function (worker) {
                ngNotify.set("You've hired worker again", 'success');
                $state.go('showWorkers');
            });
        }
        this.editWorker = function (isValid) {
            if (isValid) {
                Worker.update({
                    id: $scope.workerToEdit.id
                }, {worker: $scope.workerToEdit}).$promise.then(function (worker) {
                    ngNotify.set("You've edited worker", 'success');
                    $state.go('showWorkers');
                });
            }
        };
        $scope.filterNotArchivedWorker = function (worker) {
            return !worker.archive_worker;
        }
        $scope.filterArchivedWorker = function (worker) {
            return worker.archive_worker;
        }
        // pdf viewer events
        this.zoomInViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').zoomIn();
        };
        this.zoomOutViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').zoomOut();
        };
        this.nextPageViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').next();
        };
        this.previousPageViewer = function () {
            pdfDelegate.$getByHandle('my-pdf-container').prev();
        };
    }
]);