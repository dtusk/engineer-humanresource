'use strict';

this.appControllers.controller('PositionsController', [
    'Position', 'Qualification', 'Recruitment', '$scope', '$stateParams', '$state', 'ngNotify', function (Position, Qualification, Recruitment, $scope, $stateParams, $state, ngNotify) {
        this.init = function () {
            $scope.allQualifications = Qualification.query();
            $scope.newPosition = {};
        };
        this.createPosition = function (isValid) {
            if (isValid) {
                if ($scope.newPosition.internship == true) {
                    var tmp = $scope.newPosition
                    Position.save({
                        position: {
                            name: tmp.name + " - internship",
                            salary: tmp.salary,
                            internship: tmp.internship,
                            qualification_ids: tmp.qualification_ids
                        }
                    }).$promise.then(function (createdPosition) {
                        ngNotify.set('Position created!', 'success');
                        $scope.newPosition = {};
                    }, function (errors) {
                        $scope.errors = errors.data;
                        ngNotify.set("Position already exist", "error");
                        console.error($scope.errors);
                    });
                } else if ($scope.newPosition.internship != true) {
                    Position.save({position: $scope.newPosition}).$promise.then(function (createdPosition) {
                        ngNotify.set('Position created!', 'success');
                        console.log($scope.newPosition);
                        $scope.newPosition = {};
                    }, function (errors) {
                        $scope.errors = errors.data;
                        ngNotify.set("Position already exist", "error");
                        console.error($scope.errors);
                    });
                }
            }
        };
        this.createRecruitment = function (position) {
            Recruitment.save({recruitment: {status: true, position_id: position.id}}).$promise.then(function (createdRecruitment) {
                ngNotify.set("Recruitment opened!", 'success');
                $scope.listOfPositions = Position.query();
            }, function (errors) {
                $scope.errors = errors.data;
                console.error($scope.errors);
            });
        };
        this.showPositions = function () {
            $scope.listOfPositions = Position.query(function () {
                $scope.length = $scope.listOfPositions.length;
                $scope.listOfPositions.filter(function (position) {
                    position.qualificationNames = [];
                    position.qualifications.filter(function (qualification) {
                        position.qualificationNames.push(qualification.name)
                    });
                });
            });
            $scope.availableSearchParams = [
                {key: "name", name: "Name", placeholder: "Name..."},
                {key: "salary", name: "Salary", placeholder: "Number..."},
                {key: "internship", name: "Internship", placeholder: "True or false..."},
                {key: "qualificationNames", name: "Qualification", placeholder: "Qualification..."},
            ];
        };
        this.positionToEdit = function () {
            $scope.positionToEdit = Position.get({id: $stateParams.id});
            $scope.allQualifications = Qualification.query();
            $scope.omitSelectedQualifications = function (qualifications) {
                return $scope.allQualifications.indexOf(qualifications) === -1;
            };
        };
        this.deletePosition = function (position) {
            position.$remove (function () {
                ngNotify.set('Position has been deleted!', 'error');
                $scope.listOfPositions.splice($scope.listOfPositions.indexOf(position), 1);
            });
        };
        this.editPosition = function (isValid) {
            if (isValid) {
                Position.update({
                    id: $scope.positionToEdit.id
                }, {position: $scope.positionToEdit}).$promise.then(function (position) {
                    $scope.positionForm.$setPristine();
                    $state.go('showPositions');
                    ngNotify.set('Position saved!', 'success');
                });
            }
        };
    }
]);