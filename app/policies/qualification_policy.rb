class QualificationPolicy < ApplicationPolicy

  def index?
    true
  end

  def show?
    true
  end

  def create?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def update?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def destroy?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

end