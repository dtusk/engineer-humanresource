class PerkPolicy < ApplicationPolicy

  def index?
    (@user.hr || @user.admin || @user.boss || @user.worker) && !@user.archive_worker
  end

  def show?
    (@user.hr || @user.admin || @user.boss || @user.worker) && !@user.archive_worker
  end

  def create?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def update?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def destroy?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

end