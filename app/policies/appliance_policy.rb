class AppliancePolicy < ApplicationPolicy

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def update?
    @user.hr || @user.admin || @user.boss
  end

  def destroy?
    @user.hr || @user.admin || @user.boss
  end

end