class WorkerPolicy < ApplicationPolicy

  def index?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def show?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def get_working_times_for_user?
    @user.worker && !@user.archive_worker
  end

  def create?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def create_from_appliance?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def reset_password?
    true
  end
  def get_working_times_for_worker?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def update?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

  def destroy?
    (@user.hr || @user.admin || @user.boss) && !@user.archive_worker
  end

end
