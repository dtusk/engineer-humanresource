class Qualification < ActiveRecord::Base

  validates :name, presence: true, uniqueness: { case_sensitive: true }

  has_many :required_qualifications, dependent: :destroy
  has_many :positions, through: :required_qualifications
  has_and_belongs_to_many :workers
  has_and_belongs_to_many :appliances
end
