class Worker < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :timeoutable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :pesel, presence: true, uniqueness: { case_sensitive: false }
  validates :worker, inclusion: { in: [true, false] }
  validates :hr, inclusion: { in: [true, false] }
  validates :boss, inclusion: { in: [true, false] }
  validates :admin, inclusion: { in: [true, false] }
  validates :archive_worker, inclusion: { in: [true, false] }
  validates :post_code, presence: true
  validates :city, presence: true
  validates :house_nr, presence: true
  validates :street, presence: true

  has_many :leave, dependent: :destroy
  has_many :perks, dependent: :destroy
  has_many :schedules
  has_many :working_times, through: :schedules

  has_and_belongs_to_many :positions
  has_and_belongs_to_many :qualifications

  def cv_url
    self.cv.url
  end

  mount_uploader :cv, CvUploader



  def get_working_times
    results = []
    positions.each do |position|
      results << position.working_times
    end
    results << leave
    results.flatten
  end
end
