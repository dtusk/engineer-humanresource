class RequiredQualification < ActiveRecord::Base
  belongs_to :position
  belongs_to :qualification
end
