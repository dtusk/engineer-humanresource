class Appliance < ActiveRecord::Base

  validates :approved, inclusion: {in: [true, false]}
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :pesel, presence: true
  validates :street, presence: true
  validates :house_nr, presence: true
  validates :city, presence: true
  validates :post_code, presence: true
  validates :email, presence: true

  has_and_belongs_to_many :qualifications

  belongs_to :recruitment

  serialize :propositions_of_qualifications, Array

  def cv_path
    self.cv.url
  end

  mount_uploader :cv, CvUploader
end
