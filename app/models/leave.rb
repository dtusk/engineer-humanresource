class Leave < ActiveRecord::Base

  validates :name, presence: true
  validates :from, presence: true
  validates :to, presence: true
  validates :text, presence: true

  belongs_to :worker

  def file_url
    self.file.url
  end

  mount_uploader :file, LeaveUploader
end
