class Recruitment < ActiveRecord::Base

  validates :status, inclusion: {in: [true, false]}

  belongs_to :position

  has_many :appliances, dependent: :destroy
end
