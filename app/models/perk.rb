class Perk < ActiveRecord::Base

  validates :perks, presence: true, numericality: {greater_than_or_equal_to: 0.0}
  validates :date, presence: true
  validates :reason, presence: true

  belongs_to :worker
end
