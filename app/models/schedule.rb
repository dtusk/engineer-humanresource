class Schedule < ActiveRecord::Base
  belongs_to :position
  belongs_to :working_time
  belongs_to :worker
end
