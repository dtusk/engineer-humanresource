class Position < ActiveRecord::Base

  validates :name, presence: true
  validates :salary, presence: true, numericality: { greater_than_or_equal_to: 0.0 }
  validates :active, inclusion: { in: [true, false] }
  validates :internship, inclusion: { in: [true, false] }

  has_many :recruitments, dependent: :destroy

  has_many :required_qualifications, dependent: :destroy
  has_many :qualifications, through: :required_qualifications
  has_many :schedules, dependent: :destroy
  has_many :working_times, through: :schedules
  has_and_belongs_to_many :workers

  def active_recruitments
    self.recruitments.where(status: true)
  end
end
