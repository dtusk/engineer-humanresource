class WorkingTime < ActiveRecord::Base

  validates :date, presence: true
  validates :from, presence: true
  validates :to, presence: true

  has_many :schedules, dependent: :destroy
  has_many :positions, through: :schedules
  has_many :workers, through: :schedules
end
