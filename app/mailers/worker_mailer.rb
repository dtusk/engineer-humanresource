class WorkerMailer < ApplicationMailer
  def send_information(user, generated_password)
    @user = user
    @generated_password = generated_password
    mail(to: @user.email, from: 'auto@company.com' , subject: 'Your account had been created')
  end

  def send_resetted_password(user, generated_password)
    @user = user
    @generated_password = generated_password
    mail(to: @user.email, from: 'auto@company.com', subject: 'New password')
  end
  def send_confirmation(appliance)
    @appliance = appliance
    mail(to: @appliance.email, from: 'auto@company.com', subject: 'Appliance received')
  end
  def send_invitation(appliance)
    @appliance = appliance
    mail(to: @appliance.mail, from: 'auto@company.com', subject: 'Please visit us')
  end
end
