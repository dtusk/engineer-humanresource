class PerksSerializer < ActiveModel::Serializer

    attributes :id, :date, :reason, :perks, :worker, :worker_id
end
