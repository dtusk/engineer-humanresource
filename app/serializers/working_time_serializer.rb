class WorkingTimeSerializer < ActiveModel::Serializer

  attributes :id, :date, :from, :to, :positions, :position_ids

end
