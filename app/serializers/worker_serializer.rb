class WorkerSerializer < ActiveModel::Serializer

  attributes :id, :email, :first_name, :last_name, :pesel, :hr, :boss,
             :admin, :worker, :archive_worker, :created_at, :updated_at, :post_code,
             :street, :house_nr, :room_nr, :city, :leave_ids, :leave, :perk_ids, :perks,
             :schedule_ids, :schedules, :working_time_ids, :working_times,
             :position_ids, :positions, :qualification_ids, :qualifications, :cv_url



end
