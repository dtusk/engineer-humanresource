
class LeaveSerializer < ActiveModel::Serializer

  attributes :id, :file_url, :name, :text, :from, :to, :created_at, :updated_at, :worker_id, :approved, :worker

end
