class RecruitmentSerializer < ActiveModel::Serializer

  attributes :id, :status, :position, :created_at

end
