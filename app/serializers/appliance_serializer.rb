
class ApplianceSerializer < ActiveModel::Serializer

  attributes :id, :cv_url, :recruitment_id, :recruitment, :approved, :first_name, :last_name, :house_nr, :pesel, :street, :city, :post_code, :email, :created_at, :updated_at, :qualification_ids, :qualifications, :propositions_of_qualifications

  has_one :recruitment

end
