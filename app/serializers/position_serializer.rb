class PositionSerializer < ActiveModel::Serializer

  attributes :id, :active, :name, :salary, :internship, :qualification_ids, :qualifications, :working_time_ids, :working_times

  def active_recruitments
    object.active_recruitments
  end

end
