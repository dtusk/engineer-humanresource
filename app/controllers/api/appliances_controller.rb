class Api::AppliancesController < ApplicationController

  before_action { authorize Appliance }

  expose(:appliance) { Appliance.find(params[:id]) }

  def index
    render json: Appliance.all
  end

  def show
    render json: appliance, serializer: ApplianceSerializer
  end

  def create
    new_appliance = Appliance.new appliance_params

    if session[:attachment_id].blank?
      render json: { errors: 'Attachment id is not set' } and return
    end

    attachment = Attachment.find(session[:attachment_id])
    new_appliance.cv = attachment.file

    if new_appliance.save
      session.delete(:attachment_id)
      attachment.destroy
      WorkerMailer.send_confirmation(new_appliance).deliver_now
      render json: new_appliance, status: :created
    else
      render json: new_appliance.errors, status: :unprocessable_entity
    end
  end

  def update
    if appliance.update(appliance_params)
      render json: appliance
    else
      render json: appliance.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @appliance.destroy
  end

  private
  def appliance_params
    params.require(:appliance).permit(:cv, :recruitment_id, :approved, :first_name, :last_name, :house_nr, :pesel, :street, :city, :post_code, :position, :email, qualification_ids: [], propositions_of_qualifications: [:name])
  end

end
