class Api::AttachmentsController < ApplicationController

  before_action { authorize Attachment }

  def create
    attachment = Attachment.new(file: params[:file])

    if attachment.save
      session[:attachment_id] = attachment.id
      render json: { attachment_id: session[:attachment_id] }, status: :created
    else
      render json: attachment.errors, status: :unprocessable_entity
    end
  end

  def show
    attachment = Attachment.find(params[:id])
    render json: attachment
  end
end
