class Api::WorkingTimesController < ApplicationController

  before_action { authorize WorkingTime }

  expose(:working_time) { WorkingTime.find(params[:id]) }

  def index
    render json: WorkingTime.includes(:positions).to_json({include: :positions})
  end

  def show
    render json: working_time, serializer: WorkingTimeSerializer
  end

  def create
    new_working_time = WorkingTime.new working_time_params

    if new_working_time.save
      render json: new_working_time, status: :created
    else
      render json: new_working_time.errors, status: :unprocessable_entity
    end
  end

  def update
    if working_time.update(working_time_params)
      render json: working_time
    else
      render json: working_time.errors, status: :unprocessable_entity
    end
  end

  def destroy
    working_time.destroy
    head :no_content
  end


  private
    def working_time_params
      params.require(:working_time).permit(:date, :from, :to, position_ids: [])
    end
end
