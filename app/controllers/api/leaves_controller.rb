class Api::LeavesController < ApplicationController

  before_action { authorize Leave }

  expose(:leave) { Leave.find(params[:id]) }

  def index
    render json: Leave.includes(:worker)
  end

  def show
    render json: leave, serializer: LeaveSerializer
  end

  def create
    new_leave = Leave.new leave_params
    if session[:attachment_id].present?
      attachment = Attachment.find(session[:attachment_id])
      new_leave.file = attachment.file
    end

    if new_leave.save
      if session[:attachment_id].present?
        session.delete(:attachment_id) if session[:attachment_id]
        attachment.destroy
      end
      render json: new_leave, status: :created
    else
      render json: new_leave.errors, status: :unprocessable_entity
    end
  end

  def update
    if leave.update(leave_params)
      render json: leave
    else
      render json: leave.errors, status: :unprocessable_entity
    end
  end

  def destroy
    leave.destroy
    head :no_content
  end

  private

    def leave_params
      params.require(:leave).permit(:name, :from, :to, :approved, :worker_id, :text, :atachment)
    end
end
