class Api::WorkersController < ApplicationController

  before_action { authorize Worker }
  before_action :authenticate_worker!, except: [:reset_password]

  expose(:worker) { Worker.find(params[:id]) }

  def index
    render json: Worker.includes(:leave, :perks, :schedules, :working_times, :positions, :qualifications).as_json(include: [:leave, :perks, :schedules, :working_times, {positions: {include: :qualifications}}, :qualifications])
  end

  def show
    render json: worker, serializer: WorkerSerializer
  end

  def get_working_times_for_user
    render json: current_user.get_working_times
  end

  def get_working_times_for_worker
    render json: worker.get_working_times
  end

  def create
    new_worker = Worker.new(worker_params.merge(worker: true))

    attachment = Attachment.find_by_id(session[:attachment_id])

    new_worker.cv = attachment.file unless attachment.nil?

    generated_password = SecureRandom.urlsafe_base64(6)

    new_worker.password = generated_password
    new_worker.password_confirmation = generated_password

    if new_worker.save
      attachment.destroy unless attachment.nil?
      WorkerMailer.send_information(new_worker, generated_password).deliver_now
      render json: new_worker, status: :created, serializer: WorkerSerializer
    else
      render json: new_worker.errors, status: :unprocessable_entity
    end
  end

  def create_from_appliance
    render json: {error: 'worker[cv_url] must be provided'}, status: :unprocessable_entity and return if params[:worker][:cv_url].blank?

    appliances = Appliance.where(cv: params[:worker][:cv_url].split(/\//).last)
    appliances.to_a.delete_if { |appliance| appliance.cv.url != params[:worker][:cv_url] }

    new_worker = Worker.new(worker_params.merge(worker: true))

    generated_password = SecureRandom.urlsafe_base64(6)
    new_worker.password = generated_password
    new_worker.password_confirmation = generated_password
    new_worker.cv = appliances.first.cv

    if new_worker.save
      appliances.first.destroy
      WorkerMailer.send_information(new_worker, generated_password).deliver_now
      render json: new_worker, status: :created
    else
      render json: new_worker.errors, status: :unprocessable_entity
    end
  end

  def reset_password
    worker = Worker.find_by_pesel(worker_params[:pesel])

    head :not_found and return if worker.nil?

    generated_password = SecureRandom.urlsafe_base64(6)

    if worker.update({ password: generated_password, password_confirmation: generated_password })
      WorkerMailer.send_resetted_password(worker, generated_password).deliver_now
      head :ok
    else
      render json: worker.errors, status: :unprocessable_entity
    end
  end

  def update
    if worker.update(worker_params)
      render json: worker, serializer: WorkerSerializer
    else
      render json: worker.errors, status: :unprocessable_entity
    end
  end

  def destroy
    worker.destroy
    head :no_content
  end


  private
    def worker_params
      params[:worker][:qualification_ids] ||= [] if params[:worker].has_key?(:qualification_ids)
      params.require(:worker).permit(:first_name,:cv, :last_name, :email, :pesel, :worker, :hr, :boss, :admin, :archive_worker, :post_code, :street, :house_nr, :room_nr, :city, position_ids: [], qualification_ids: [])
    end
end
