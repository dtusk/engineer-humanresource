class Api::RequiredQualifactionsController < ApplicationController


  before_action { authorize RequiredQualification }

  expose(:required_qualification) { RequiredQualification.find(params[:id]) }

  def index
    render json: RequiredQualification.all
  end

  def show
    render json: required_qualification
  end

  def create
    new_required_qualification = RequiredQualification.new required_qualifications_params

    if new_required_qualification.save
      render json: new_required_qualification, status: :created
    else
      render json: new_required_qualification.errors, status: :unprocessable_entity
    end
  end

  def update
    if required_qualification.update required_qualifications_params
      render json: required_qualification
    else
      render json: required_qualification.errors, status: :unprocessable_entity
    end
  end

  def destroy
    required_qualification.destroy
    head :no_content
  end
   

  private
    def required_qualifications_params
      params.require(:required_qualification).permit(:position_id, :from, :to, :status)
    end
end
