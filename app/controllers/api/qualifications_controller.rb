class Api::QualificationsController < ApplicationController

  before_action { authorize Qualification }

  expose(:qualification) { Qualification.find(params[:id]) }

  def index
    render json: Qualification.all
  end

  def show
    render json: qualification
  end

  def create
    new_qualification = Qualification.new qualification_params

    if new_qualification.save
      render json: new_qualification, status: :created
    else
      render json: new_qualification.errors, status: :unprocessable_entity
    end
  end

  def update
    if qualification.update(qualification_params)
      render json: qualification
    else
      render json: qualification.errors, status: :unprocessable_entity
    end
  end

  def destroy
    qualification.destroy
    head :no_content
  end
   

  private
    def qualification_params
      params.require(:qualification).permit(:name, appliance_ids: [])
    end
end
