class Api::HeartbeatController < ApplicationController

  before_filter :skip_authorization

  def alive
    head :ok
  end

  def signed_in
    if request.env['warden'] && request.env['warden'].authenticated?
      head :ok
    else
      head 403
    end
  end
end
