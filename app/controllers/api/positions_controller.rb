class Api::PositionsController < ApplicationController

  before_action { authorize Position }

  expose(:position) { Position.find(params[:id]) }

  def index
    render json: Position.includes(:qualifications, :recruitments, :workers).as_json({include: [:qualifications, :recruitments, :workers], methods: :active_recruitments})
  end

  def show
    render json: position, serializer: PositionSerializer
  end

  def create
    new_position = Position.new position_params

    if new_position.save
      render json: new_position, status: :created
    else
      render json: new_position.errors, status: :unprocessable_entity
    end
  end

  def update
    if position.update(position_params)
      render json: position, serializer: PositionSerializer
    else
      render json: position.errors, status: :unprocessable_entity
    end
  end

  def destroy
    position.destroy
    head :no_content
  end

  private
    def position_params
      params[:position][:qualification_ids] ||= [] if params[:position].has_key?(:qualification_ids)
      params.require(:position).permit(:name, :salary, :active, :internship, qualification_ids: [])
    end
end
