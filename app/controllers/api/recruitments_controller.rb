class Api::RecruitmentsController < ApplicationController

  before_action { authorize Recruitment }

  expose(:recruitment) { Recruitment.find(params[:id]) }

  def index
    render json: Recruitment.includes(:position).to_json(include: :position)
  end

  def show
    render json: recruitment.to_json(include: :position)
  end

  def create
    new_recruitment = Recruitment.new recruitment_params

    if new_recruitment.save
      render json: new_recruitment, status: :created
    else
      render json: new_recruitment.errors, status: :unprocessable_entity
    end
  end

  def update
    if recruitment.update(recruitment_params)
      render json: recruitment
    else
      render json: recruitment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    recruitment.destroy
    head :no_content
  end


  private
    def recruitment_params
      params.require(:recruitment).permit(:status, :position_id)
    end
end
