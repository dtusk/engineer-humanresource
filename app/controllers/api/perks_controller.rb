class Api::PerksController < ApplicationController

  before_action { authorize Perk }

  expose(:perk) { Perk.find(params[:id]) }

  def index
    render json: Perk.all.as_json({include: :worker})
  end
  
  def show
    render json: perk, serializer: PerksSerializer
  end

  def create
    new_perk = Perk.new perk_params

    if new_perk.save
      render json: new_perk, status: :created
    else
      render json: new_perk.errors, status: :unprocessable_entity
    end
  end

  def update
    if perk.update(perk_params)
      render json: perk
    else
      render json: perk.errors, status: :unprocessable_entity
    end
  end

  def destroy
    perk.destroy
    head :no_content
  end

  private
    def perk_params
      params.require(:perk).permit(:perks, :date, :reason, :workers, :worker_id)
    end
end
