class Api::SchedulesController < ApplicationController

  before_action { authorize Schedule }

  expose(:schedule) { Schedule.find(params[:id]) }

  def index
    render json: Schedule.all
  end

  def show
    render json: schedule
  end

  def create
    new_schedule = Schedule.new schedules_params

    if new_schedule.save
      render json: new_schedule, status: :created
    else
      render json: new_schedule.errors, status: :unprocessable_entity
    end
  end

  def update
    if schedule.update schedules_params
      render json: schedule
    else
      render json: schedule.errors, status: :unprocessable_entity
    end
  end

  def destroy
    schedule.destroy
    head :no_content
  end
   

  private
    def schedules_params
      params.require(:schedule).permit(:position_id, :worker_id, :working_time_id)
    end
end