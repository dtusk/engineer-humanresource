class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  include Pundit

  protect_from_forgery with: :null_session

  after_action :verify_authorized, unless: :devise_controller?

  skip_before_filter  :verify_authenticity_token

  def index
    skip_authorization
  end

  def default_serializer_options
    {root: false}
  end

  protected

  	def auth_worker
  	  render plain: 'You are not allowed to run this request', status: :unauthorized unless worker_signed_in?
  	end

  	def verified_request?
  	  if respond_to?(:valid_authenticity_token?, true)
  	    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  	  else
  	  	super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
  	  end
  	end

    def current_user
      current_worker
    end
end
